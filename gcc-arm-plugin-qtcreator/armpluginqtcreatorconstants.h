#ifndef ARMPLUGINQTCREATORCONSTANTS_H
#define ARMPLUGINQTCREATORCONSTANTS_H
#include <QString>
#include <QDomElement>
#include <QDomDocument>

namespace ArmPluginQtCreator {
namespace Constants {

const char ADD_INCLUDE_PATHS_ACTION_ID[] = "ArmPluginQtCreator.AddIncludePathAction";
const char ADD_EXISTING_FILES_ACTION_ID[] = "ArmPluginQtCreator.AddExistingFilesAction";
const char PROJECT_PROPERTIES_ACTION_ID[] = "ArmPluginQtCreator.ArmProjectPropertiesAction";
const char MENU_ID[] = "ArmPluginQtCreator.Menu";

const char ARM_PROJECT_MIME_TYPE[] = "application/arm-plugin-qtcretor-project";
const char PROJECTCONTEXT[]     = "ArmProject.ProjectContext";

// Project
const char ARM_PROJECT_ID[]  = "ArmProjectManager.ArmProject";
const char PROJECT_FILE_ID[] = "ArmProjectManager.ProjectFile";

//Build
const char ARM_PROJECT_BC_ID[] = "ArmProjectManager.ArmProjectBuildConfiguration";

//icons
const char HEADER_ICON[] = ":/images/headers.png";
const char SOURCE_ICON[] = ":/images/sources.png";

} // namespace ArmPluginQtCreator
} // namespace Constants

#endif // ARMPLUGINQTCREATORCONSTANTS_H

