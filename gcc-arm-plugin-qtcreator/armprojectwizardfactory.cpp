//#include "armprojectwizardfactory.h"
//#include <coreplugin/coreconstants.h>
//#include <projectexplorer/projectexplorerconstants.h>
//#include <projectexplorer/customwizard/customwizard.h>
//#include <projectexplorer/projectfilewizardextension.h>
//#include <projectexplorer/kitinformation.h>
//#include "Utils.h"
//#include <QDir>

//const char ARM_PROJECT_FACTORY[] = "ArmProjectManager.ArmProjectWizardFactory";
//namespace ArmPluginQtCreator {
//namespace Internal {

//ArmProjectWizard::ArmProjectWizard(QWidget *parent, const Core::WizardDialogParameters &parameters)
//    : ProjectExplorer::BaseProjectWizardDialog(parent, parameters),
//      m_targetSetupPage(new ProjectExplorer::TargetSetupPage(this))
//{
//    connect(this, SIGNAL(projectParametersChanged(QString,QString)), this, SLOT(onProjectParameterChanged()));
//    setWindowTitle(QLatin1String("Arm project wizard"));

//    m_targetSetupPage->setTitle(QLatin1String("Choose Target"));
//    m_targetSetupPage->setRequiredKitMatcher(ProjectExplorer::KitMatcher(kitMatcher));
//    this->addPage(m_targetSetupPage);
//}

//ArmProjectWizard::~ArmProjectWizard()
//{

//}

//void ArmProjectWizard::onProjectParameterChanged()
//{
//    QString projectFilePath = QFileInfo(path(), projectName() + QLatin1String(".chops")).absoluteFilePath();
//    m_targetSetupPage->setProjectPath(projectFilePath);
//}

//ArmProjectWizardFactory::ArmProjectWizardFactory()
//{
//    setWizardKind(ProjectWizard);
//    setDisplayName(tr("Gnu GCC arm project"));
//    setId(Core::Id(ARM_PROJECT_FACTORY));
//    setDescription(tr("Create arm project using gnu gcc compiler. "
//                      "No need to manage makefile, Project management will be done itself"));
//    setCategory(QLatin1String(ProjectExplorer::Constants::QT_PROJECT_WIZARD_CATEGORY));
//    setDisplayCategory(QLatin1String(ProjectExplorer::Constants::QT_PROJECT_WIZARD_CATEGORY_DISPLAY));
//    setFlags(Core::IWizardFactory::PlatformIndependent);
//}

//ArmProjectWizardFactory::~ArmProjectWizardFactory()
//{

//}

//Core::BaseFileWizard *ArmProjectWizardFactory::create(QWidget *parent, const Core::WizardDialogParameters &parameters) const
//{
//    ArmProjectWizard* wizard = new ArmProjectWizard(parent, parameters);
//    foreach (QWizardPage *p, parameters.extensionPages())
//        wizard->addPage(p);
//    return wizard;
//}

//Core::GeneratedFiles ArmProjectWizardFactory::generateFiles(const QWizard *w, QString *errorMessage) const
//{
//    Q_UNUSED(errorMessage)
//    QList<Core::GeneratedFile> files;
//    const ArmProjectWizard *wizard = qobject_cast<const ArmProjectWizard *>(w);
//    const QString projectPath = wizard->path();
//    const QDir dir(projectPath);
//    const QString projectName = wizard->projectName();
//    const QString projectFile = QFileInfo(dir, projectName + QLatin1String(".chops")).absoluteFilePath();

//    Core::GeneratedFile generatedChopsFile(projectFile);
//    generatedChopsFile.setContents(QString());
//    generatedChopsFile.setAttributes(Core::GeneratedFile::OpenProjectAttribute);
//    files.append(generatedChopsFile);
//    return files;
//}

//bool ArmProjectWizardFactory::postGenerateFiles(const QWizard *w, const Core::GeneratedFiles &l, QString *errorMessage)
//{
//    Q_UNUSED(w);
//    return ProjectExplorer::CustomProjectWizard::postGenerateOpen(l, errorMessage);
//}

//} // namespace Internal
//} // namespace ArmPluginQtCreator
