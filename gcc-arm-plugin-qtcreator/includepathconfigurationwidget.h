#ifndef INCLUDEPATHCONFIGURATIONWIDGET_H
#define INCLUDEPATHCONFIGURATIONWIDGET_H

#include <QWidget>
#include <QStringListModel>
#include <QDialog>

namespace Ui {
class IncludePathConfigurationWidget;
}

namespace ArmPluginQtCreator {
namespace Internal {

class IncludePathsModel : public QStringListModel
{
    Q_OBJECT

public:
    IncludePathsModel(QObject* parent, bool editable);
    ~IncludePathsModel();
    Qt::ItemFlags flags(const QModelIndex &index) const;

private:
    bool m_editable;
};


class StringListManagementWidget : public QWidget
{
    Q_OBJECT

public:
    explicit StringListManagementWidget(QWidget *parent , bool isDirectoryList = true);
    ~StringListManagementWidget();

    void setStringList(const QStringList& stringList);
    QStringList stringList() const;

private slots:
    void on_pbAdd_clicked();

    void on_pbRemove_clicked();

private:
    Ui::IncludePathConfigurationWidget *m_ui;
    IncludePathsModel* m_model;
    QString m_lastPath;
    bool m_isDirectoryList;
};

class IncludePathConfigurationDialog : public QDialog
{
    Q_OBJECT

public:
    IncludePathConfigurationDialog(QWidget* parent = 0);
    ~IncludePathConfigurationDialog();

    void setIncludePaths(const QStringList& includePaths);
    QStringList includePaths() const;

private:
    StringListManagementWidget* m_widget;
};

} // namespace Internal
} // namespace ArmProjectManager
#endif // INCLUDEPATHCONFIGURATIONWIDGET_H
