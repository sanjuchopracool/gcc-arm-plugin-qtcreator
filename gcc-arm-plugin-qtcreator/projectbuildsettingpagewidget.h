#ifndef PROJECTBUILDSETTINGPAGEWIDGET_H
#define PROJECTBUILDSETTINGPAGEWIDGET_H

#include <QWidget>
#include "ISettingPageWidget.h"
#include "armprojectbuildsetting.h"

namespace Ui {
class ProjectBuildSettingPage;
}

namespace ArmPluginQtCreator {
namespace Internal {

class ComboSelectionUtilityWidget;
class ProjectBuildSettingPageWidget : public QWidget,
        public ISettingPageWidget
{
    Q_OBJECT

public:
    explicit ProjectBuildSettingPageWidget(ArmProjectBuildSetting& buildSetting, QWidget *parent = 0);
    ~ProjectBuildSettingPageWidget();
    QWidget* widget();
    void apply();

private:
    void init();
    void connectTargetPageConnections();
    void connectCPageConnections();
    void connectLinkerPageConnections();
    ArmTargetSetting targetSettingFromUi() const;
    AssemblerSetting assemblerSettingFromUi() const;
    CCompilationSetting cCompilationSettingFromUi() const;
    LinkerSetting linkerSettingFromUi() const;
    void refreshAssemblerPageCommand();
    void refreshCCompilationPageCommand();
    void refreshLinkerPageCommand();

private slots:
    void slotCurrenWidgetChanged();
    void slotTargetSettingChanged();
    void slotAssemblerSettingChanged();
    void slotCSettingSettingChanged();
    void slotLinkerSettingChanged();

    void on_toolButtonLAdd_clicked();

    void on_toolButtonLRemove_clicked();

private:
    Ui::ProjectBuildSettingPage *ui;
    ArmTargetSetting m_cpuTargetSetting;
    AssemblerSetting m_assemblerSetting;
    CCompilationSetting m_cCompilationSetting;
    LinkerSetting m_linkerSetting;
    ArmProjectBuildSetting &m_projectBuildSetting;
};

} // namespace Internal
} // namespace ArmProjectManage

#endif // PROJECTBUILDSETTINGPAGEWIDGET_H
