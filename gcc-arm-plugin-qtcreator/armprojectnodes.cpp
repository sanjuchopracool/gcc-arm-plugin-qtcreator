#include "armprojectnodes.h"
#include "armproject.h"
#include "armpluginqtcreatorconstants.h"
#include <coreplugin/fileiconprovider.h>

namespace ArmPluginQtCreator {
namespace Internal {


QIcon headerFolderIcon()
{
    return QIcon(Core::FileIconProvider::overlayIcon(QStyle::SP_DirIcon,
                                                     QIcon(QLatin1String(Constants::HEADER_ICON)),
                                                     QSize(16, 16)));
}

QIcon sourceFolderIcon()
{
    return QIcon(Core::FileIconProvider::overlayIcon(QStyle::SP_DirIcon,
                                                     QIcon(QLatin1String(Constants::SOURCE_ICON)),
                                                     QSize(16, 16)));
}

ArmProjectNode::ArmProjectNode(ArmProject *project)
    : ProjectNode(project->projectDirectory()),
      m_project(project),
      m_headerNode(0),
      m_sourcesNode(0),
      m_otherNode(0)
{
    m_projectDirectoyString = project->projectDirectory().toString();
    this->setDisplayName(project->displayName());
}

ArmProjectNode::~ArmProjectNode()
{
}

bool ArmProjectNode::canAddSubProject(const QString &proFilePath) const
{
    Q_UNUSED(proFilePath)
    return false;
}

bool ArmProjectNode::addSubProjects(const QStringList &proFilePaths)
{
    Q_UNUSED(proFilePaths)
    return false;
}

bool ArmProjectNode::removeSubProjects(const QStringList &proFilePaths)
{
    Q_UNUSED(proFilePaths)
    return false;
}

QList<ProjectExplorer::ProjectAction> ArmProjectNode::supportedActions(ProjectExplorer::Node *node) const
{
    QList<ProjectExplorer::ProjectAction> actions;
    ArmProjectNode* projectNode = static_cast<ArmProjectNode*>(node);
    if(projectNode)
        actions << ProjectExplorer::AddNewFile;

    return actions;
}

bool ArmProjectNode::addFiles(const QStringList &filePaths, QStringList *notAdded)
{
    Q_UNUSED(notAdded)
    QStringList headerFiles;
    QStringList sourceFiles;
    QStringList otherFiles;
    filterFilesBasedOnExtensions(filePaths, headerFiles, sourceFiles, otherFiles);
    m_project->addOrRemoveHeaderFiles(headerFiles);
    m_project->addOrRemoveSourceFiles(sourceFiles);
    m_project->addOrRemoveOtherFiles(otherFiles);
    return true;
}

bool ArmProjectNode::removeFiles(const QStringList &filePaths, QStringList *notRemoved)
{
    Q_UNUSED(notRemoved)
    QStringList headerFiles;
    QStringList sourceFiles;
    QStringList otherFiles;
    filterFilesBasedOnExtensions(filePaths, headerFiles, sourceFiles, otherFiles);
    m_project->addOrRemoveHeaderFiles(headerFiles, false);
    m_project->addOrRemoveSourceFiles(sourceFiles, false);
    m_project->addOrRemoveOtherFiles(otherFiles, false);
    return true;
}

bool ArmProjectNode::deleteFiles(const QStringList &filePaths)
{
    qDebug() << filePaths;
    return false;
}

bool ArmProjectNode::renameFile(const QString &filePath, const QString &newFilePath)
{
    return m_project->renameFile(filePath, newFilePath);
}

void ArmProjectNode::createOrDeleteHeaderNode(bool create)
{
    if(create)
    {
        if(!m_headerNode)
        {
            m_headerNode = new MyVirtualNode(m_projectDirectoyString, 2);
            m_headerNode->setDisplayName(QLatin1String("Headers"));
            m_headerNode->setIcon(headerFolderIcon());
            this->addFolderNodes(QList<ProjectExplorer::FolderNode*>() << m_headerNode);
        }
    }
    else
    {
        if(m_headerNode)
            removeFolderNodes(QList<ProjectExplorer::FolderNode*>() << m_headerNode);
        m_headerNode = 0;
    }
}

void ArmProjectNode::createOrDeleteSourceNode(bool create)
{
    if(create)
    {
        if(!m_sourcesNode)
        {
            m_sourcesNode = new MyVirtualNode(m_projectDirectoyString, 1);
            m_sourcesNode->setDisplayName(QLatin1String("Sources"));
            m_sourcesNode->setIcon(sourceFolderIcon());
            this->addFolderNodes(QList<ProjectExplorer::FolderNode*>() << m_sourcesNode);
        }
    }
    else
    {
        if(m_sourcesNode)
            removeFolderNodes(QList<ProjectExplorer::FolderNode*>() << m_sourcesNode);
        m_sourcesNode = 0;
    }
}

void ArmProjectNode::createOrDeleteOtherNode(bool create)
{
    if(create)
    {
        if(!m_otherNode)
        {
            m_otherNode = new MyVirtualNode(m_projectDirectoyString, 0);
            m_otherNode->setDisplayName(QLatin1String("Others"));
            this->addFolderNodes(QList<ProjectExplorer::FolderNode*>() << m_otherNode);
        }
    }
    else
    {
        if(m_otherNode)
            removeFolderNodes(QList<ProjectExplorer::FolderNode*>() << m_otherNode);
        m_otherNode = 0;
    }
}

void ArmProjectNode::addOrRemoveHeaderFiles(const QStringList &headerFiles, bool add)
{
    //headerFiles will definately have count
    if(add)
        createOrDeleteHeaderNode(); //make sure its always there

    addOrRemoveFilesForTheseDirectory(m_headerIncludeDirectoryNodes, m_headerNode,
                                 headerFiles,ProjectExplorer::FileType::Header, add);
}

void ArmProjectNode::addOrRemoveSourceFiles(const QStringList &sourceFiles, bool add)
{
    if(add)
        createOrDeleteSourceNode(); //make sure its always there

    addOrRemoveFilesForTheseDirectory(m_SourcesFilesDirectoryNodes, m_sourcesNode,
                                 sourceFiles,ProjectExplorer::FileType::Source, add);
}

void ArmProjectNode::addOrRemoveOtherFiles(const QStringList &otherFiles, bool add)
{
    if(add)
        createOrDeleteOtherNode(); //make sure its always there

    addOrRemoveFilesForTheseDirectory(m_OthersFilesDirectoryNodes, m_otherNode,
                                 otherFiles,ProjectExplorer::FileType::Unknown, add);
}

void ArmProjectNode::filterFilesBasedOnExtensions(const QStringList &filesToFilter,
                                                  QStringList &headers,
                                                  QStringList &sources,
                                                  QStringList &others) const
{
    Q_FOREACH(QString fileName, filesToFilter)
    {
        QString suffix = QFileInfo(fileName).suffix();
        if(suffix == QLatin1String("c") || suffix == QLatin1String("cpp") ||
                suffix == QLatin1String("S")|| suffix == QLatin1String("s"))
            sources.append(fileName);
        else if(suffix == QLatin1String("h") || suffix == QLatin1String("hpp"))
            headers.append(fileName);
        else
            others.append(fileName);
    }
}

ProjectExplorer::FileType ArmProjectNode::fileTypeFromExtension(const QString &filePath)
{
    QString suffix = QFileInfo(filePath).suffix();
    if(suffix == QLatin1String("c") || suffix == QLatin1String("cpp") ||
            suffix == QLatin1String("S")|| suffix == QLatin1String("s"))
        return ProjectExplorer::FileType::Source;

    if(suffix == QLatin1String("h") || suffix == QLatin1String("hpp"))
        return ProjectExplorer::FileType::Header;

    return ProjectExplorer::FileType::Unknown;
}

QStringList ArmProjectNode::includePathForAddedFiles() const
{
    return m_headerIncludeDirectoryNodes.keys();
}

void ArmProjectNode::createFolderHash(QHash<QString, QStringList> &folderFilesHash, const QStringList &files) const
{
    Q_FOREACH(QString file, files)
    {
        QFileInfo fileInfo(file);
        folderFilesHash[fileInfo.absoluteDir().path()].append(file);
    }
}

void ArmProjectNode::addOrRemoveFilesForTheseDirectory(QMap<QString, ProjectExplorer::FolderNode *> &directoryNodesMap,
                                                       ProjectExplorer::FolderNode *parentNode,
                                                       const QStringList &files,
                                                       ProjectExplorer::FileType type,
                                                       bool add)
{
    QHash<QString, QStringList> folderFilesHash; //key is folder and values i files in it
    createFolderHash(folderFilesHash, files);
    QStringList projectDirectoryFiles = folderFilesHash.value(m_projectDirectoyString);
    folderFilesHash.remove(m_projectDirectoyString);
    Q_FOREACH(QString folderPath, folderFilesHash.keys())
    {
        ProjectExplorer::FolderNode* folderNode = directoryNodesMap.value(folderPath, 0);
        if(add)
        {
            if(!folderNode)
            {
                folderNode = new ProjectExplorer::FolderNode(Utils::FileName::fromString(folderPath));
                folderNode->setDisplayName(m_project->relativeFilePath(folderPath));
                if(parentNode == m_headerNode)
                    folderNode->setIcon(headerFolderIcon());
                else if(parentNode == m_sourcesNode)
                    folderNode->setIcon(sourceFolderIcon());

                parentNode->addFolderNodes(QList<ProjectExplorer::FolderNode*>() << folderNode);
                directoryNodesMap.insert(folderPath, folderNode);
            }
            QList<ProjectExplorer::FileNode*> fileNodes;
            Q_FOREACH(QString fileName, folderFilesHash.value(folderPath))
                fileNodes.append(new ArmProjectFileNode(fileName, type));
            folderNode->addFileNodes(fileNodes);
        }
        else
        {
            if(folderNode)
            {
                QStringList filesToRemove = folderFilesHash.value(folderPath);
                QList<ProjectExplorer::FileNode*> fileNodes = folderNode->fileNodes();
                QList<ProjectExplorer::FileNode*> fileNodesToRemove;
                Q_FOREACH(ProjectExplorer::FileNode* fileNode, fileNodes)
                {
                    if(filesToRemove.contains(fileNode->filePath().toString()))
                        fileNodesToRemove.append(fileNode);
                }

                folderNode->removeFileNodes(fileNodesToRemove);
                //again check if there is no file remove it
                fileNodes = folderNode->fileNodes();
                if(fileNodes.isEmpty())
                {
                    parentNode->removeFolderNodes(QList<ProjectExplorer::FolderNode*>() << folderNode);
                    directoryNodesMap.remove(folderPath);
                }
            }
        }
    }

    if(projectDirectoryFiles.count())
    {
        if(add)
        {
            QList<ProjectExplorer::FileNode*> fileNodes;
            Q_FOREACH(QString fileName, projectDirectoryFiles)
                fileNodes.append(new ArmProjectFileNode(fileName, type));
            parentNode->addFileNodes(fileNodes);
        }
        else
        {
            QList<ProjectExplorer::FileNode*> fileNodes = parentNode->fileNodes();
            QList<ProjectExplorer::FileNode*> fileNodesToRemove;
            Q_FOREACH(ProjectExplorer::FileNode* fileNode, fileNodes)
            {
                if(projectDirectoryFiles.contains(fileNode->filePath().toString()))
                    fileNodesToRemove.append(fileNode);
            }
            parentNode->removeFileNodes(fileNodesToRemove);
        }
    }

    //check if need to delete parent
    if(parentNode->fileNodes().isEmpty() && parentNode->folderNodes().isEmpty())
    {
        if(parentNode == m_headerNode)
            createOrDeleteHeaderNode(false);
        else if(parentNode == m_sourcesNode)
            createOrDeleteSourceNode(false);
        else if(parentNode == m_otherNode)
            createOrDeleteOtherNode(false);
    }
}


IncludePathNode::IncludePathNode(const QString &folderPath)
    : VirtualFolderNode(Utils::FileName::fromString(QDir::toNativeSeparators(folderPath)), 3)
{
    setIcon(QIcon(headerFolderIcon()));
}

IncludePathNode::~IncludePathNode()
{

}

QList<ProjectExplorer::ProjectAction> IncludePathNode::supportedActions(ProjectExplorer::Node *node) const
{
    QList<ProjectExplorer::ProjectAction> actions = ProjectExplorer::VirtualFolderNode::supportedActions(node);
    actions.removeOne(ProjectExplorer::AddExistingFile);
    actions.removeOne(ProjectExplorer::AddNewFile);
    return actions;
}

void IncludePathNode::updateFiles()
{
    QStringList filters;
    filters << QLatin1String("*.h") << QLatin1String("*.hpp");
    QFileInfoList fileInfoList = QDir(QDir::toNativeSeparators(this->filePath().toString())).entryInfoList(filters, QDir::Files);
    QList<ProjectExplorer::FileNode*> headerFileNodes;
    Q_FOREACH(QFileInfo fileInfo, fileInfoList)
    {
        headerFileNodes.append(new ArmProjectHeaderFile(fileInfo.absoluteFilePath()));
    }

    this->addFileNodes(headerFileNodes);
}

ArmProjectHeaderFile::ArmProjectHeaderFile(const QString &path)
    : ProjectExplorer::FileNode(Utils::FileName::fromString(path), ProjectExplorer::FileType::Header, false)
{
}

QList<ProjectExplorer::ProjectAction> ArmProjectHeaderFile::supportedActions(ProjectExplorer::Node *node) const
{
    QList<ProjectExplorer::ProjectAction> actions = ProjectExplorer::FileNode::supportedActions(node);
    actions << ProjectExplorer::RemoveFile << ProjectExplorer::Rename;
    return actions;
}

ArmProjectHeaderFile::~ArmProjectHeaderFile()
{

}


//////////////////////////////////////////////////////////////////
/// \brief ArmProjectFileNode::ArmProjectFileNode
/// \param path
////////////////////////////////////////////////////////////////////

ArmProjectFileNode::ArmProjectFileNode(const QString &path,ProjectExplorer::FileType type)
    : ProjectExplorer::FileNode(Utils::FileName::fromString(path), type, false)
{

}

QList<ProjectExplorer::ProjectAction> ArmProjectFileNode::supportedActions(ProjectExplorer::Node *node) const
{
    QList<ProjectExplorer::ProjectAction> actions = ProjectExplorer::FileNode::supportedActions(node);
    actions << ProjectExplorer::RemoveFile << ProjectExplorer::Rename;
    return actions;
}

ArmProjectFileNode::~ArmProjectFileNode()
{

}

////////////////////////////////////////////////////////////////////////////////////
/// \brief MyVirtualNode::MyVirtualNode
/// \param folderPath
/// \param priority
//////////////////////////////////////////////////////////////////////////////////


MyVirtualNode::MyVirtualNode(const QString &folderPath, int priority)
    :VirtualFolderNode(Utils::FileName::fromString(folderPath), priority)
{

}

MyVirtualNode::~MyVirtualNode()
{

}

QList<ProjectExplorer::ProjectAction> MyVirtualNode::supportedActions(ProjectExplorer::Node *node) const
{
    QList<ProjectExplorer::ProjectAction> actions = ProjectExplorer::VirtualFolderNode::supportedActions(node);
    actions.removeOne(ProjectExplorer::AddExistingFile);
    actions.removeOne(ProjectExplorer::AddNewFile);
    return actions;
}

} // namespace Internal
} // namespace ArmProjectManager
