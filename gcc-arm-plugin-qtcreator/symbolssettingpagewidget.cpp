#include "symbolssettingpagewidget.h"
#include "armproject.h"

namespace ArmPluginQtCreator {
namespace Internal {

SymbolsSettingPageWidget::SymbolsSettingPageWidget(ArmProjectBuildSetting &buildSetting, QWidget *parent)
    : StringListManagementWidget(parent, false),
      m_buildSetting(buildSetting),
      m_icon(QIcon(QLatin1String(":/images/hash.png")))
{
    this->setStringList(m_buildSetting.symbols());
}

SymbolsSettingPageWidget::~SymbolsSettingPageWidget()
{

}

QString SymbolsSettingPageWidget::name() const
{
    return tr("Symbols");
}

const QIcon &SymbolsSettingPageWidget::icon() const
{
    return m_icon;
}

QWidget *SymbolsSettingPageWidget::widget()
{
    return this;
}

void SymbolsSettingPageWidget::apply()
{
    m_buildSetting.setSymbols(this->stringList());
}

} // namespace Internal
} // namespace ArmProjectManager
