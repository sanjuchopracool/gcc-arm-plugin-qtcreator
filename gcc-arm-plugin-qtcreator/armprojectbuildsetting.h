#ifndef ARMPROJECTBUILDSETTING_H
#define ARMPROJECTBUILDSETTING_H

#include <QObject>
#include <QDir>
#include <QDomDocument>
#include <QDomElement>

namespace ArmPluginQtCreator {
namespace Internal {

class ArmProject;

struct ArmToolChain
{
    //////////////////////////////////////////////////////////////////////////////////////////
    /// \Toolcahin setting will depend on the kit user have chosen from the qtcreator setting
    /// no need to save this information, it will change when ever we will add a new kit
    //////////////////////////////////////////////////////////////////////////////////////////

    QDir directory;
    QString prefix;
    QString cCompiler() const;
    QString cppCompiler() const;
    QString asembler() const;
    QString linker() const;
    QString objdump() const;
    QString objcopy() const;
    QString size() const;

    bool operator == (const ArmToolChain& other) const;
    bool operator != (const ArmToolChain& other) const;

};

struct ArmTargetSetting
{
    ArmTargetSetting()
        : cpu(QLatin1String("cortex-m4")),
          instructionSet(QLatin1String("-mthumb")),
          useInterWorking(false),
          floatAbi(QLatin1String("hard")),
          fpu(QLatin1String("fpv4-sp-d16"))
    {

    }
    QString cpu;
    QString instructionSet;
    bool useInterWorking;
    QString floatAbi;
    QString fpu;

    bool operator == (const ArmTargetSetting& other) const;
    bool operator != (const ArmTargetSetting& other) const;
    QString flags() const;

    //saving and loading
    void save(QDomElement& rootE, QDomDocument& doc);
    void load(const QDomElement& rootE, const QDomDocument& doc);
};

//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

struct AssemblerSetting {

    AssemblerSetting()
        : debugFlag(QLatin1String("-g")),
          showWarning(true)
    {

    }

    QString debugFlag;
    bool showWarning;
    QString flags() const;

    bool operator == (const AssemblerSetting& other) const;
    bool operator != (const AssemblerSetting& other) const;

    //saving and loading
    void save(QDomElement& rootE, QDomDocument& doc);
    void load(const QDomElement& rootE, const QDomDocument& doc);
};
///////////////////////////////////////////////////////////////////////////////////////////////
/// /////////////////////////////////////////////////////////////////////////////////////////////

struct CCompilationSetting
{
    CCompilationSetting()
        : debugFlag(QLatin1String("-g")),
          optimizationFlag(QLatin1String("-Os")),
          showAllWarnings(true),
          removeDeadCode(true),
          removeDeadData(true),
          cStd(QLatin1String("c99")),
          cppStd(QLatin1String("c++14"))
    {

    }

    QString debugFlag;
    QString optimizationFlag;
    bool showAllWarnings;
    bool removeDeadCode;
    bool removeDeadData;
    QString extraFlags;
    QString cStd;
    QString cppStd;

    QString flags() const;
    QString cFlags() const;
    QString cppFlags() const;
    bool operator == (const CCompilationSetting& other) const;
    bool operator != (const CCompilationSetting& other) const;

    //saving and loading
    void save(QDomElement& rootE, QDomDocument& doc);
    void load(const QDomElement& rootE, const QDomDocument& doc);
};
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

struct LinkerSetting
{
    LinkerSetting()
        : removeDeadCode(true),
          doNotUseSharedLib(true),
          doNotUseStartUpFile(false),
          doNotUseDefaultLib(false),
          doNotUseStandardLib(false),
          linkWithNewLibNano(false),
          extraFlags(QLatin1String("-Wl,-cref,-u,Reset_Handler"))
    {

    }

    QString linkerFile;
    bool removeDeadCode;
    bool doNotUseSharedLib;
    bool doNotUseStartUpFile;
    bool doNotUseDefaultLib;
    bool doNotUseStandardLib;
    bool linkWithNewLibNano;
    QString extraFlags;
    QStringList staticLibraries;
    QString projectFileDir;

    QString flags() const;
    bool operator == (const LinkerSetting& other) const;
    bool operator != (const LinkerSetting& other) const;

    //saving and loading
    void save(QDomElement& rootE, QDomDocument& doc);
    void load(const QDomElement& rootE, const QDomDocument& doc);
};
////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////

struct OtherSetting
{
    OtherSetting()
        : createBin(true),
          createHex(true)
    {

    }
    bool createBin;
    bool createHex;

    bool operator == (const OtherSetting &other) const;
    bool operator != (const OtherSetting& other) const;

    //saving and loading
    void save(QDomElement& rootE, QDomDocument& doc);
    void load(const QDomElement& rootE, const QDomDocument& doc);
};

//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

struct ArmProjectBuildSettingData;
class ArmProjectBuildSetting : public QObject
{
    Q_OBJECT
public:
    explicit ArmProjectBuildSetting(ArmProject *project = 0);
    ~ArmProjectBuildSetting();

    ArmProject* project() const; //if have project return it
    const ArmToolChain &toolChain() const;
    const ArmTargetSetting &targetCpuSetting() const;
    const AssemblerSetting &assemblerSetting() const;
    const CCompilationSetting &cCompilationSetting() const;
    const LinkerSetting &linkerSetting() const;
    const OtherSetting &otherSetting() const;
    const QStringList& symbols() const;
    const QStringList &userIncludePaths() const;

    void setTargetSetting(const ArmTargetSetting &setting);
    void setAssemblerSetting(const AssemblerSetting &setting);
    void setCCompilationSetting(const CCompilationSetting &setting);
    void setLinkerSetting(const LinkerSetting &setting);
    void setOtherSetting(const OtherSetting& setting);
    void setSymbols(const QStringList &symbols);
    void setUserIncludePaths(const QStringList& includePaths);

    //saving and loading
    void save(QDomElement& rootE, QDomDocument& doc);
    void load(const QDomElement& rootE, const QDomDocument& doc);
    //directory for project wizard
    void setRefrenceDirectory(const QString& path);

signals:
    void settingModified();
    void includePathsModified();

public slots:
    void updateToolChain();

private:
    ArmProjectBuildSettingData* d;
};

} // namespace Internal
} // namespace ArmProjectManager

#endif // ARMPROJECTBUILDSETTING_H
