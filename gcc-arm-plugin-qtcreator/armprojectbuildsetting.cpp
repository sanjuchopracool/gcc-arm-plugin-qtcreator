#include "armprojectbuildsetting.h"

#include <QFileInfo>

#ifdef PROJECT_IMPORTER
#include <QDebug>
#else
#include "armproject.h"
#include <projectexplorer/target.h>
#include <projectexplorer/toolchain.h>
#include <projectexplorer/kitinformation.h>

using namespace ProjectExplorer;
#endif
namespace ArmPluginQtCreator {
namespace Internal {

//we will always use unix style seperator

////////////////////////////////////////////////////////////////
/// ToolChain
////////////////////////////////////////////////////////////////

QString ArmToolChain::cCompiler() const
{
    return directory.absoluteFilePath(prefix + QLatin1String("gcc"));
}

QString ArmToolChain::cppCompiler() const
{
    return directory.absoluteFilePath(prefix + QLatin1String("g++"));
}

QString ArmToolChain::asembler() const
{
    return directory.absoluteFilePath(prefix + QLatin1String("as"));
}

QString ArmToolChain::linker() const
{
    return directory.absoluteFilePath(prefix + QLatin1String("ld"));
}

QString ArmToolChain::objdump() const
{
    return directory.absoluteFilePath(prefix + QLatin1String("objdump"));
}

QString ArmToolChain::objcopy() const
{
    return directory.absoluteFilePath(prefix + QLatin1String("objcopy"));
}

QString ArmToolChain::size() const
{
    return directory.absoluteFilePath(prefix + QLatin1String("size"));
}

bool ArmToolChain::operator ==(const ArmToolChain &other) const
{
    return (directory == other.directory) &&
            (prefix == other.prefix);
}

bool ArmToolChain::operator !=(const ArmToolChain &other) const
{
    return !(*this == other);
}

//////////////////////////////////////////////////////////////////////////
/// \brief ArmTargetSetting::operator ==
/// \param other
/// \return
/////////////////////////////////////////////////////////////////////////

bool ArmTargetSetting::operator ==(const ArmTargetSetting &other) const
{
    return (cpu == other.cpu) &&
            (instructionSet == other.instructionSet) &&
            (useInterWorking == other.useInterWorking) &&
            (floatAbi == other.floatAbi) &&
            (fpu == other.fpu);
}

bool ArmTargetSetting::operator !=(const ArmTargetSetting &other) const
{
    return !(*this == other);
}

QString ArmTargetSetting::flags() const
{
    QStringList flags;
    QLatin1Char space(' ');
    if(!cpu.isEmpty())
        flags << (QLatin1String("-mcpu=") + cpu);

    if(!instructionSet.isEmpty())
        flags << instructionSet;

    if(useInterWorking)
        flags  << QLatin1String("-mthumb-interwork");

    flags << (QLatin1String("-mfloat-abi=") + floatAbi);

    if(!fpu.isEmpty())
        flags <<  (QLatin1String("-mfpu=") + fpu);

    return flags.join(space);
}

void ArmTargetSetting::save(QDomElement &rootE, QDomDocument &doc)
{
    QDomElement settingE = doc.createElement(QLatin1String("TargetSetting"));

    QDomElement cpuE = doc.createElement(QLatin1String("cpu"));
    cpuE.appendChild(doc.createTextNode(cpu));
    settingE.appendChild(cpuE);

    QDomElement instructionSetE = doc.createElement(QLatin1String("instructionSet"));
    instructionSetE.appendChild(doc.createTextNode(instructionSet));
    settingE.appendChild(instructionSetE);

    QDomElement useInterWorkingE = doc.createElement(QLatin1String("useInterWorking"));
    useInterWorkingE.appendChild(doc.createTextNode(QString::number(useInterWorking)));
    settingE.appendChild(useInterWorkingE);

    QDomElement floatAbiE = doc.createElement(QLatin1String("floatAbi"));
    floatAbiE.appendChild(doc.createTextNode(floatAbi));
    settingE.appendChild(floatAbiE);

    QDomElement fpuE = doc.createElement(QLatin1String("fpu"));
    fpuE.appendChild(doc.createTextNode(fpu));
    settingE.appendChild(fpuE);

    rootE.appendChild(settingE);
}

void ArmTargetSetting::load(const QDomElement &rootE, const QDomDocument &doc)
{
    Q_UNUSED(doc)
    QDomElement settingE = rootE.firstChildElement(QLatin1String("TargetSetting"));

    QDomElement cpuE = settingE.firstChildElement(QLatin1String("cpu"));
    cpu = cpuE.text();

    QDomElement instructionSetE = settingE.firstChildElement(QLatin1String("instructionSet"));
    instructionSet = instructionSetE.text();

    QDomElement useInterWorkingE = settingE.firstChildElement(QLatin1String("useInterWorking"));
    useInterWorking = useInterWorkingE.text().toInt();

    QDomElement floatAbiE = settingE.firstChildElement(QLatin1String("floatAbi"));
    floatAbi = floatAbiE.text();

    QDomElement fpuE = settingE.firstChildElement(QLatin1String("fpu"));
    fpu = fpuE.text();
}

///////////////////////////////////////////////////////////////////////////////////////////////
/// /////////////////////////////////////////////////////////////////////////////////////////////

QString AssemblerSetting::flags() const
{
    QString flags;
    QLatin1Char space(' ');
    if(!debugFlag.isEmpty())
        flags = debugFlag + space;

    flags += QLatin1String("-Wa,"); //to tell that gcc that these options are for assembler
    if(showWarning)
        flags += QLatin1String("--warn");
    else
        flags += QLatin1String("--no-warn ");

    flags += space + QLatin1String("-x assembler-with-cpp");
    return flags;
}

bool AssemblerSetting::operator ==(const AssemblerSetting &other) const
{
    return (debugFlag == other.debugFlag) &&
            (showWarning == other.showWarning);
}

bool AssemblerSetting::operator !=(const AssemblerSetting &other) const
{
    return !(*this == other);
}

void AssemblerSetting::save(QDomElement &rootE, QDomDocument &doc)
{
    QDomElement settingE = doc.createElement(QLatin1String("AssemblerSetting"));

    QDomElement debugE = doc.createElement(QLatin1String("debugFlag"));
    debugE.appendChild(doc.createTextNode(debugFlag));
    settingE.appendChild(debugE);

    QDomElement showWarningE = doc.createElement(QLatin1String("showWarning"));
    showWarningE.appendChild(doc.createTextNode(QString::number(showWarning)));
    settingE.appendChild(showWarningE);

    rootE.appendChild(settingE);
}

void AssemblerSetting::load(const QDomElement &rootE, const QDomDocument &doc)
{
    Q_UNUSED(doc)
    QDomElement settingE = rootE.firstChildElement(QLatin1String("AssemblerSetting"));

    QDomElement debugE = settingE.firstChildElement(QLatin1String("debugFlag"));
    debugFlag = debugE.text();

    QDomElement showWarningE = settingE.firstChildElement(QLatin1String("showWarning"));
    showWarning = showWarningE.text().toInt();
}
//////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////

bool CCompilationSetting::operator == (const CCompilationSetting& other) const
{
    return (debugFlag == other.debugFlag) &&
            (optimizationFlag == other.optimizationFlag) &&
            (showAllWarnings == other.showAllWarnings) &&
            (removeDeadCode == other.removeDeadCode) &&
            (removeDeadData == other.removeDeadData) &&
            (extraFlags == other.extraFlags) &&
            (cStd == other.cStd) &&
            (cppStd == other.cppStd);
}

QString CCompilationSetting::flags() const
{
    QString flags;
    QLatin1Char space(' ');
    if(!optimizationFlag.isEmpty())
        flags = optimizationFlag + space;

    if(removeDeadCode)
        flags += QLatin1String("-ffunction-sections") + space;

    if(removeDeadData)
        flags += QLatin1String("-fdata-sections") + space;

    if(!debugFlag.isEmpty())
        flags += debugFlag + space;

    if(showAllWarnings)
        flags += QLatin1String("-Wall") + space;

    if(!extraFlags.isEmpty())
        flags += extraFlags;

    return flags;
}

QString CCompilationSetting::cFlags() const
{
    QString retFlags = flags();
    if(!cStd.isEmpty())
        retFlags += QLatin1Char(' ') + QLatin1String("-std=") + cStd;

    qDebug() << retFlags;
    return retFlags;
}

QString CCompilationSetting::cppFlags() const
{
    QString retFlags = flags();
    retFlags += QLatin1String(" -fno-exceptions ") + QLatin1String(" -fno-rtti ");
    if(!cppStd.isEmpty())
        retFlags += QLatin1Char(' ') + QLatin1String("-std=") + cppStd;

    qDebug() << retFlags;
    return retFlags;
}

bool CCompilationSetting::operator != (const CCompilationSetting& other) const
{
    return !(*this == other);
}

void CCompilationSetting::save(QDomElement &rootE, QDomDocument &doc)
{
    QDomElement settingE = doc.createElement(QLatin1String("CCompilationSetting"));

    QDomElement debugE = doc.createElement(QLatin1String("debugFlag"));
    debugE.appendChild(doc.createTextNode(debugFlag));
    settingE.appendChild(debugE);

    QDomElement optimizationE = doc.createElement(QLatin1String("optimizationFlag"));
    optimizationE.appendChild(doc.createTextNode(optimizationFlag));
    settingE.appendChild(optimizationE);

    QDomElement showWarningE = doc.createElement(QLatin1String("showAllWarnings"));
    showWarningE.appendChild(doc.createTextNode(QString::number(showAllWarnings)));
    settingE.appendChild(showWarningE);

    QDomElement removeDeadCodeE = doc.createElement(QLatin1String("removeDeadCode"));
    removeDeadCodeE.appendChild(doc.createTextNode(QString::number(removeDeadCode)));
    settingE.appendChild(removeDeadCodeE);

    QDomElement removeDeadDataE = doc.createElement(QLatin1String("removeDeadData"));
    removeDeadDataE.appendChild(doc.createTextNode(QString::number(removeDeadData)));
    settingE.appendChild(removeDeadDataE);

    QDomElement extraFlagE = doc.createElement(QLatin1String("extraFlags"));
    extraFlagE.appendChild(doc.createTextNode(extraFlags));
    settingE.appendChild(extraFlagE);

    QDomElement cStdE = doc.createElement(QLatin1String("cStd"));
    cStdE.appendChild(doc.createTextNode(cStd));
    settingE.appendChild(cStdE);

    QDomElement cppStdE = doc.createElement(QLatin1String("CppStd"));
    cppStdE.appendChild(doc.createTextNode(cppStd));
    settingE.appendChild(cppStdE);

    rootE.appendChild(settingE);
}

void CCompilationSetting::load(const QDomElement &rootE, const QDomDocument &doc)
{
    Q_UNUSED(doc)
    QDomElement settingE = rootE.firstChildElement(QLatin1String("CCompilationSetting"));

    QDomElement optimizationFlagE = settingE.firstChildElement(QLatin1String("optimizationFlag"));
    optimizationFlag = optimizationFlagE.text();

    QDomElement debugE = settingE.firstChildElement(QLatin1String("debugFlag"));
    debugFlag = debugE.text();

    QDomElement showWarningE = settingE.firstChildElement(QLatin1String("showAllWarnings"));
    showAllWarnings = showWarningE.text().toInt();

    QDomElement removeDeadCodeE = settingE.firstChildElement(QLatin1String("removeDeadCode"));
    removeDeadCode = removeDeadCodeE.text().toInt();

    QDomElement removeDeadDataE = settingE.firstChildElement(QLatin1String("removeDeadData"));
    removeDeadData = removeDeadDataE.text().toInt();

    QDomElement extraFlagE = settingE.firstChildElement(QLatin1String("extraFlags"));
    extraFlags = extraFlagE.text();

    QDomElement cStdE = settingE.firstChildElement(QLatin1String("cStd"));
    cStd = cStdE.text();

    QDomElement cppStdE = settingE.firstChildElement(QLatin1String("CppStd"));
    cppStd = cppStdE.text();
}
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

QString LinkerSetting::flags() const
{
    QLatin1Char space(' ');
    QString flags;
    if(!linkerFile.isEmpty())
        flags += QLatin1String("-T") + linkerFile + space;

    flags += QLatin1String("-Wl,--start-group") + space;
    if(linkWithNewLibNano)
        flags += QLatin1String("-lc_s");
    else
        flags +=  QLatin1String("-lc");

    flags += space;
    flags += QLatin1String("-lm") + space;
    flags += QLatin1String("-Wl,--end-group") + space;
    if(doNotUseStartUpFile)
        flags += QLatin1String("-nostartfiles") + space;
    if(doNotUseDefaultLib)
        flags += QLatin1String("-nodefaultlibs") + space;
    if(doNotUseStandardLib)
        flags += QLatin1String("-nostdlib") + space;
    if(doNotUseSharedLib)
        flags += QLatin1String("-static") + space;

    Q_FOREACH(const QString& lib, staticLibraries)
        flags += lib + space;

    flags += extraFlags + space;
    if(removeDeadCode)
        flags += QLatin1String(" -Wl,--gc-sections");

    return flags;
}

bool LinkerSetting::operator ==(const LinkerSetting &other) const
{
    bool flag = (linkerFile == other.linkerFile) &&
            (removeDeadCode == other.removeDeadCode) &&
            (doNotUseSharedLib == other.doNotUseSharedLib) &&
            (doNotUseStartUpFile == other.doNotUseStartUpFile) &&
            (doNotUseDefaultLib == other.doNotUseDefaultLib) &&
            (doNotUseStandardLib == other.doNotUseStandardLib) &&
            (linkWithNewLibNano == other.linkWithNewLibNano) &&
            (extraFlags == other.extraFlags);

    if(!flag)
        return false;
    QStringList localList = staticLibraries;
    localList.sort();
    QStringList otherList = other.staticLibraries;
    otherList.sort();

    return (localList == otherList);
}

bool LinkerSetting::operator !=(const LinkerSetting &other) const
{
    return !(*this == other);
}

void LinkerSetting::save(QDomElement &rootE, QDomDocument &doc)
{
    QDomElement settingE = doc.createElement(QLatin1String("LinkerSetting"));

    QDomElement linkerFileE = doc.createElement(QLatin1String("linkerFile"));

    QString linkerFileRelativePath;
    linkerFileRelativePath = QDir(projectFileDir).relativeFilePath(linkerFile);
    linkerFileE.appendChild(doc.createTextNode(linkerFileRelativePath));
    settingE.appendChild(linkerFileE);

    QDomElement removeDeadCodeE = doc.createElement(QLatin1String("removeDeadCode"));
    removeDeadCodeE.appendChild(doc.createTextNode(QString::number(removeDeadCode)));
    settingE.appendChild(removeDeadCodeE);

    QDomElement doNotUseSharedLibE = doc.createElement(QLatin1String("doNotUseSharedLib"));
    doNotUseSharedLibE.appendChild(doc.createTextNode(QString::number(doNotUseSharedLib)));
    settingE.appendChild(doNotUseSharedLibE);

    QDomElement doNotUseStartUpFileE = doc.createElement(QLatin1String("doNotUseStartUpFile"));
    doNotUseStartUpFileE.appendChild(doc.createTextNode(QString::number(doNotUseStartUpFile)));
    settingE.appendChild(doNotUseStartUpFileE);

    QDomElement doNotUseDefaultLibE = doc.createElement(QLatin1String("doNotUseDefaultLib"));
    doNotUseDefaultLibE.appendChild(doc.createTextNode(QString::number(doNotUseDefaultLib)));
    settingE.appendChild(doNotUseDefaultLibE);

    QDomElement doNotUseStandardLibE = doc.createElement(QLatin1String("doNotUseStandardLib"));
    doNotUseStandardLibE.appendChild(doc.createTextNode(QString::number(doNotUseStandardLib)));
    settingE.appendChild(doNotUseStandardLibE);

    QDomElement linkWithNanoLibE = doc.createElement(QLatin1String("linkWithNewLibNano"));
    linkWithNanoLibE.appendChild(doc.createTextNode(QString::number(linkWithNewLibNano)));
    settingE.appendChild(linkWithNanoLibE);

    QDomElement extraFlagE = doc.createElement(QLatin1String("extraFlags"));
    extraFlagE.appendChild(doc.createTextNode(extraFlags));
    settingE.appendChild(extraFlagE);

    QDomElement staticLibrariesE = doc.createElement(QLatin1String("staticLibraries"));
    staticLibrariesE.appendChild(doc.createTextNode(staticLibraries.join(QLatin1Char(';'))));
    settingE.appendChild(staticLibrariesE);

    rootE.appendChild(settingE);
}

void LinkerSetting::load(const QDomElement &rootE, const QDomDocument &doc)
{
    Q_UNUSED(doc)
    QDomElement settingE = rootE.firstChildElement(QLatin1String("LinkerSetting"));

    QDomElement linkerFileE = settingE.firstChildElement(QLatin1String("linkerFile"));
    linkerFile = QDir::cleanPath(QDir(projectFileDir).absoluteFilePath(linkerFileE.text()));

    QDomElement removeDeadCodeE = settingE.firstChildElement(QLatin1String("removeDeadCode"));
    removeDeadCode = removeDeadCodeE.text().toInt();

    QDomElement doNotUseSharedLibE = settingE.firstChildElement(QLatin1String("doNotUseSharedLib"));
    doNotUseSharedLib = doNotUseSharedLibE.text().toInt();

    QDomElement doNotUseStartUpFileE = settingE.firstChildElement(QLatin1String("doNotUseStartUpFile"));
    doNotUseStartUpFile = doNotUseStartUpFileE.text().toInt();

    QDomElement doNotUseDefaultLibE = settingE.firstChildElement(QLatin1String("doNotUseDefaultLib"));
    doNotUseDefaultLib = doNotUseDefaultLibE.text().toInt();

    QDomElement doNotUseStandardLibE = settingE.firstChildElement(QLatin1String("doNotUseStandardLib"));
    doNotUseStandardLib = doNotUseStandardLibE.text().toInt();

    QDomElement linkWithNanoLibE = settingE.firstChildElement(QLatin1String("linkWithNewLibNano"));
    linkWithNewLibNano = linkWithNanoLibE.text().toInt();

    QDomElement extraFlagE = settingE.firstChildElement(QLatin1String("extraFlags"));
    if(!extraFlagE.isNull())
        extraFlags = extraFlagE.text();

    QDomElement staticLibrariesE = settingE.firstChildElement(QLatin1String("staticLibraries"));
    staticLibraries = staticLibrariesE.text().split(QLatin1Char(';'));
}

/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////

bool OtherSetting::operator == (const OtherSetting& other) const
{
    return (createBin == other.createBin) &&
            (createHex == other.createHex);
}

bool OtherSetting::operator !=(const OtherSetting &other) const
{
    return !(*this == other);
}

void OtherSetting::save(QDomElement &rootE, QDomDocument &doc)
{
    QDomElement settingE = doc.createElement(QLatin1String("OtherSetting"));

    QDomElement createBinE = doc.createElement(QLatin1String("createBin"));
    createBinE.appendChild(doc.createTextNode(QString::number(createBin)));
    settingE.appendChild(createBinE);

    QDomElement createHexE = doc.createElement(QLatin1String("createHex"));
    createHexE.appendChild(doc.createTextNode(QString::number(createHex)));
    settingE.appendChild(createHexE);

    rootE.appendChild(settingE);
}

void OtherSetting::load(const QDomElement &rootE, const QDomDocument &doc)
{
    Q_UNUSED(doc)
    QDomElement settingE = rootE.firstChildElement(QLatin1String("OtherSetting"));
    if(settingE.isNull())
        return;

    QDomElement createBinE = settingE.firstChildElement(QLatin1String("createBin"));
    createBin = createBinE.text().toInt();

    QDomElement createHexE = settingE.firstChildElement(QLatin1String("createHex"));
    createHex = createHexE.text().toInt();
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/// ArmProjectBuildSetting
///////////////////////////////////////////////////////////////////////////////////////////////////

struct ArmProjectBuildSettingData
{
    ArmProjectBuildSettingData()
        : project(0)
    {

    }

    ArmToolChain toolChain;
    ArmProject* project;
    ArmTargetSetting targetSetting;
    AssemblerSetting assemblerSetting;
    CCompilationSetting cCompilationSetting;
    LinkerSetting linkerSetting;
    OtherSetting otherSetting;
    QStringList symbols;
    QStringList userIncludePath;
    QString refrenceDirectoryPath;
};

ArmProjectBuildSetting::ArmProjectBuildSetting(ArmProject *project)
#ifndef PROJECT_IMPORTER
    : QObject(project)
#endif
{
    d = new ArmProjectBuildSettingData;
    d->project = project;
}

ArmProjectBuildSetting::~ArmProjectBuildSetting()
{
    delete d;
}

ArmProject *ArmProjectBuildSetting::project() const
{
    return d->project;
}

const ArmToolChain &ArmProjectBuildSetting::toolChain() const
{
    return d->toolChain;
}

const ArmTargetSetting &ArmProjectBuildSetting::targetCpuSetting() const
{
    return d->targetSetting;
}

const AssemblerSetting &ArmProjectBuildSetting::assemblerSetting() const
{
    return d->assemblerSetting;
}

const CCompilationSetting &ArmProjectBuildSetting::cCompilationSetting() const
{
    return d->cCompilationSetting;
}

const LinkerSetting &ArmProjectBuildSetting::linkerSetting() const
{
    return d->linkerSetting;
}

const OtherSetting &ArmProjectBuildSetting::otherSetting() const
{
    return d->otherSetting;
}

void ArmProjectBuildSetting::setTargetSetting(const ArmTargetSetting &setting)
{
    if(d->targetSetting != setting)
    {
        d->targetSetting = setting;
        qDebug() << d->targetSetting.flags();
        emit settingModified();
    }
}

void ArmProjectBuildSetting::setAssemblerSetting(const AssemblerSetting &setting)
{
    if(d->assemblerSetting != setting)
    {
        d->assemblerSetting = setting;
        qDebug() << d->assemblerSetting.flags();
        emit settingModified();
    }
}

void ArmProjectBuildSetting::setCCompilationSetting(const CCompilationSetting &setting)
{
    if(d->cCompilationSetting != setting)
    {
        d->cCompilationSetting = setting;
        qDebug() << d->cCompilationSetting.flags();
        emit settingModified();
    }
}

void ArmProjectBuildSetting::setLinkerSetting(const LinkerSetting &setting)
{
    if(d->linkerSetting != setting)
    {
        d->linkerSetting = setting;
        qDebug() << d->linkerSetting.flags();
        emit settingModified();
    }
}

void ArmProjectBuildSetting::setOtherSetting(const OtherSetting &setting)
{
    if(d->otherSetting != setting)
    {
        d->otherSetting = setting;
        emit settingModified();
    }
}

void ArmProjectBuildSetting::setSymbols(const QStringList &symbols)
{
    d->symbols.sort();
    QStringList newSymbols = symbols;
    newSymbols.sort();
    if(d->symbols != newSymbols)
    {
        d->symbols = symbols;
        emit settingModified();
    }
}

void ArmProjectBuildSetting::setUserIncludePaths(const QStringList &includePaths)
{
    d->userIncludePath.sort();
    QStringList newPathsList = includePaths;
    newPathsList.sort();
    if(d->userIncludePath != newPathsList)
    {
        d->userIncludePath = includePaths;
        emit includePathsModified();
    }
}

const QStringList &ArmProjectBuildSetting::symbols() const
{
    return d->symbols;
}

const QStringList& ArmProjectBuildSetting::userIncludePaths() const
{
    return d->userIncludePath;
}

void ArmProjectBuildSetting::save(QDomElement &rootE, QDomDocument &doc)
{
    QDomElement settingE = doc.createElement(QLatin1String("BuildSettings"));
    d->targetSetting.save(settingE, doc);
    d->assemblerSetting.save(settingE, doc);
    d->cCompilationSetting.save(settingE, doc);

#ifndef PROJECT_IMPORTER
    d->linkerSetting.projectFileDir = d->project->projectDirectory().toString();
#endif

    d->linkerSetting.save(settingE, doc);

    d->otherSetting.save(settingE, doc);
    //save symbols
    QDomElement symbolsE = doc.createElement(QLatin1String("Symbols"));
    Q_FOREACH(const QString& symbol, d->symbols)
    {
        QDomElement symbolE = doc.createElement(QLatin1String("Symbol"));
        symbolE.setAttribute(QLatin1String("value"), symbol);
        symbolsE.appendChild(symbolE);
    }
    settingE.appendChild(symbolsE);

    QStringList relativePaths;
#ifndef PROJECT_IMPORTER
    if(d->project)
        relativePaths = d->project->relativeFilePaths(d->userIncludePath);
#else
    {
        QDir refrenceDirectory(d->refrenceDirectoryPath);
        Q_FOREACH(const QString& path, d->userIncludePath)
            relativePaths.append(refrenceDirectory.relativeFilePath(path));
    }
#endif
    QDomElement includePathsE = doc.createElement(QLatin1String("IncludePaths"));
    Q_FOREACH(const QString& symbol, relativePaths)
    {
        QDomElement includePathE = doc.createElement(QLatin1String("IncludePath"));
        includePathE.setAttribute(QLatin1String("value"), symbol);
        includePathsE.appendChild(includePathE);
    }

    settingE.appendChild(includePathsE);

    rootE.appendChild(settingE);
}

void ArmProjectBuildSetting::load(const QDomElement &rootE, const QDomDocument &doc)
{
    Q_UNUSED(doc)
#ifndef PROJECT_IMPORTER
    QDomElement settingE = rootE.firstChildElement(QLatin1String("BuildSettings"));
    d->targetSetting.load(settingE, doc);
    d->assemblerSetting.load(settingE, doc);
    d->cCompilationSetting.load(settingE, doc);
    //to resolve linker file path
    d->linkerSetting.projectFileDir = d->project->projectDirectory().toString();
    d->linkerSetting.load(settingE, doc);

    d->otherSetting.load(settingE, doc);
    //load symbols
    QDomElement symbolsE = settingE.firstChildElement(QLatin1String("Symbols"));
    if(!symbolsE.isNull())
    {
        QDomElement symbolE = symbolsE.firstChildElement(QLatin1String("Symbol"));
        while (!symbolE.isNull())
        {
            d->symbols.append(symbolE.attribute(QLatin1String("value")));
            symbolE = symbolE.nextSiblingElement();
        }
    }

    QDomElement includePathsE = settingE.firstChildElement(QLatin1String("IncludePaths"));
    if(!includePathsE.isNull())
    {
        QDomElement includePathE = includePathsE.firstChildElement(QLatin1String("IncludePath"));
        while (!includePathE.isNull())
        {
            d->userIncludePath.append(d->project->absoluteFilePath(includePathE.attribute(QLatin1String("value"))));
            includePathE = includePathE.nextSiblingElement();
        }

        emit includePathsModified();
    }   
#endif

}

void ArmProjectBuildSetting::setRefrenceDirectory(const QString &path)
{
    d->refrenceDirectoryPath = path;
    d->linkerSetting.projectFileDir = path;
}

void ArmProjectBuildSetting::updateToolChain()
{
#ifndef PROJECT_IMPORTER
    Target* target = d->project->activeTarget();
    if(!target)
        return;

    ToolChain* toolChain = ToolChainKitInformation::toolChain(target->kit(), ToolChain::Language::Cxx);
    if(!toolChain)
        return;

    QString cppCompiler = toolChain->compilerCommand().toString();
    QFileInfo compileFileInfo(cppCompiler);
    ArmToolChain newToolChain;
    QString prefix = compileFileInfo.baseName();
    prefix.remove(prefix.length() - 3, 3); //remove g++ from compiler name and extract prefix
    newToolChain.directory = compileFileInfo.dir();
    newToolChain.prefix = prefix;
    if(d->toolChain != newToolChain)
    {
        d->toolChain = newToolChain;
        qDebug() << newToolChain.directory << newToolChain.prefix;
    }
#endif
}

} // namespace Internal
} // namespace ArmProjectManager
