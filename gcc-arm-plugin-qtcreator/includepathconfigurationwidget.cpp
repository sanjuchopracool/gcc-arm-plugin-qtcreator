#include "includepathconfigurationwidget.h"
#include "ui_includepathconfigurationwidget.h"
#include <QFileDialog>
#include <QVBoxLayout>
#include <QDialogButtonBox>

namespace ArmPluginQtCreator {
namespace Internal {

IncludePathsModel::IncludePathsModel(QObject *parent, bool editable)
    : QStringListModel(parent),
      m_editable(editable)
{

}

IncludePathsModel::~IncludePathsModel()
{

}

Qt::ItemFlags IncludePathsModel::flags(const QModelIndex &index) const
{
    Q_UNUSED(index);
    Qt::ItemFlags flags = Qt::ItemIsSelectable | Qt::ItemIsEnabled;
    if(m_editable)
        flags |= Qt::ItemIsEditable;

    return flags;
}

StringListManagementWidget::StringListManagementWidget(QWidget *parent, bool isDirectoryList) :
    QWidget(parent),
    m_ui(new Ui::IncludePathConfigurationWidget),
    m_isDirectoryList(isDirectoryList)
{
    m_ui->setupUi(this);
    setWindowTitle(tr("Manage include paths"));
    m_model = new IncludePathsModel(this, true); //editable
    m_ui->listView->setModel(m_model);
    m_lastPath = QDir::homePath();
}

StringListManagementWidget::~StringListManagementWidget()
{
    delete m_ui;
}

void StringListManagementWidget::setStringList(const QStringList &includePaths)
{
    m_model->setStringList(includePaths);
    QModelIndex index = m_model->index(0);
    m_ui->listView->setCurrentIndex(index);
}

QStringList StringListManagementWidget::stringList() const
{
    return m_model->stringList();
}

void StringListManagementWidget::on_pbAdd_clicked()
{
    QString path = QLatin1String("DEFINE");
    if(m_isDirectoryList)
        path = QFileDialog::getExistingDirectory(this, tr("Select directory"), m_lastPath, QFileDialog::ReadOnly);
    if(!path.isEmpty())
    {
        QDir lastDir(path);
        lastDir.cdUp();
        m_lastPath = lastDir.absolutePath();
        QStringList itemsList = m_model->stringList();
        if(!itemsList.contains(path))
        {
            int count = itemsList.count();
            m_model->insertRow(count);
            QModelIndex index = m_model->index(count);
            m_model->setData(index, path, Qt::EditRole);
            m_ui->listView->setCurrentIndex(index);
        }
    }
}

void StringListManagementWidget::on_pbRemove_clicked()
{
    int currentRow = m_ui->listView->currentIndex().row();
    if(currentRow >= 0)
        m_model->removeRow(currentRow);
}

IncludePathConfigurationDialog::IncludePathConfigurationDialog(QWidget *parent)
    : QDialog(parent)
{
    setWindowTitle(tr("Manage Inculde Paths"));
    m_widget = new StringListManagementWidget(this);
    QVBoxLayout* mainLayout = new QVBoxLayout;
    mainLayout->addWidget(m_widget);
    QDialogButtonBox* buttonBox = new
            QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                             Qt::Horizontal, this);
    mainLayout->addWidget(buttonBox);
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
    this->setLayout(mainLayout);
}

IncludePathConfigurationDialog::~IncludePathConfigurationDialog()
{
    delete m_widget;
}

void IncludePathConfigurationDialog::setIncludePaths(const QStringList &includePaths)
{
    m_widget->setStringList(includePaths);
}

QStringList IncludePathConfigurationDialog::includePaths() const
{
    return m_widget->stringList();
}

} // namespace Internal
} // namespace ArmProjectManager
