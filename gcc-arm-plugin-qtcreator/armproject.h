#ifndef ARMPROJECT_H
#define ARMPROJECT_H

#include <projectexplorer/project.h>
#include <coreplugin/idocument.h>
#include <projectexplorer/kit.h>
#include "armpluginqtcreatorconstants.h"
#include "armprojectbuildsetting.h"
#include <projectexplorer/targetsetuppage.h>
#include <QFuture>

namespace ArmPluginQtCreator {
namespace Internal {

class ArmProjectManager;
class ArmProjectNode;
class ArmProjectFileDocument;
class IncludePathNode;

class ArmProject : public ProjectExplorer::Project
{
    Q_OBJECT

public:
    ArmProject(ArmProjectManager *manager, const QString &filename);
    ~ArmProject();

    //ProjectExplorer::Project
    QString displayName() const;
    ProjectExplorer::IProjectManager *projectManager() const;
    ProjectExplorer::ProjectNode *rootProjectNode() const;
    QStringList files(FilesMode fileMode) const;

    void manageIncludePaths();
    void addFiles(const QStringList& files);
    void showSettingDialog();

    void addOrRemoveHeaderFiles(const QStringList& headerFiles, bool add = true);
    void addOrRemoveSourceFiles(const QStringList& sourceFiles, bool add = true);
    void addOrRemoveOtherFiles(const QStringList& otherFiles, bool add = true);
    bool renameFile(const QString &filePath, const QString &newFilePath);
    bool supportsKit(ProjectExplorer::Kit *k, QString *errorMessage = 0) const;

    //function to be used in build steps
    QStringList allIncludePaths() const;
    QStringList sourceFiles() const;
    QStringList linkerFiles() const;

    //after build
    void updateApplicationTargets();

    //resolve paths
    QString relativeFilePath(const QString& absolutePath) const;
    QString absoluteFilePath(const QString& relativePath) const;
    QStringList relativeFilePaths(const QStringList& absoluteFilePaths) const;
    QStringList absoluteFilePaths(const QStringList& relativeFilePaths) const;


    //build Setting
    ArmProjectBuildSetting &projectBuildSetting();

protected:
    RestoreResult fromMap(const QVariantMap &map, QString *errorMessage);

private:
    ArmProjectManager* m_projectManager;
    QString m_fileName;
    QString m_projectName;
    QDir m_projectDir;
    ArmProjectNode *m_rootNode;
    QMap<QString, IncludePathNode*> m_userIncludePathNodeMap;
    QStringList m_headerFiles;
    QStringList m_sourceFiles;
    QStringList m_otherFiles;

    //Project Setting it will be the real one
    ArmProjectBuildSetting m_projectBuildSetting;
    QFuture<void> m_codeModelFuture;

signals:
    void modified();

private:
    void loadProject();
    void connectPostLoadingConnections();
    QStringList filterExisitingFiles(const QStringList& newFiles, const QStringList& exisitngFiles, bool add = true);
    void addOrRemoveListInExisitingList(QStringList& existingList, const QStringList& changes, bool add = true);
    void filterLists(const QStringList& exisitingList, const QStringList& toAdd,
                     QStringList& added, QStringList& removed);

private slots:
    void saveProject();
    void updateCodeEditorTools();
    void slotUserIncludePathsModified();
};

class ArmProjectFileDocument : public Core::IDocument
{
public:
    ArmProjectFileDocument(ArmProject *parent, QString fileName);
    ~ArmProjectFileDocument();

    QString defaultPath() const;
    QString suggestedFileName() const;
    bool isModified() const;
    bool isSaveAsAllowed() const;
    bool save(QString *errorString, const QString &fileName = QString(), bool autoSave = false);
    bool reload(QString *errorString, ReloadFlag flag, ChangeType type);

private:
    ArmProject* m_project;
};

} // namespace Internal
} // namespace ArmProjectManager

#endif // ARMPROJECT_H
