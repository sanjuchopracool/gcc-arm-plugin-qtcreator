#include "armprojectmanager.h"
#include "armpluginqtcreatorconstants.h"
#include "armproject.h"

#include <QFileInfo>

namespace ArmPluginQtCreator {
namespace Internal {

ArmProjectManager::ArmProjectManager() :
    ProjectExplorer::IProjectManager()
{
}

QString ArmProjectManager::mimeType() const
{
    return QLatin1String(Constants::ARM_PROJECT_MIME_TYPE);
}

ProjectExplorer::Project *ArmProjectManager::openProject(const QString &fileName, QString *errorString)
{
    if (!QFileInfo(fileName).isFile()) {
        if (errorString)
            *errorString = tr("Failed opening project \"%1\": Project is not a file.")
                .arg(fileName);
        return 0;
    }

    return new ArmProject(this, fileName);
}

void ArmProjectManager::registerProject(ArmProject *project)
{
    m_projects.append(project);
}

void ArmProjectManager::unregisterProject(ArmProject *project)
{
    m_projects.removeAll(project);
}

} // namespace Internal
} // namespace ArmProjectManager
