DEFINES += ARMPLUGINQTCREATOR_LIBRARY
QT += xml
# ArmPluginQtCreator files

SOURCES += armpluginqtcreatorplugin.cpp \
    armprojectmanager.cpp \
    armproject.cpp \
    armprojectnodes.cpp \
    includepathconfigurationwidget.cpp \
    armprojectbuildconfigurationfactory.cpp \
    armprojectgenericstep.cpp \
    armprojectwizardfactory.cpp \
    armprojectsettingdialog.cpp \
    includepathsettingpagewidget.cpp \
    symbolssettingpagewidget.cpp \
    armprojectbuildsetting.cpp \
    toolchaininfopage.cpp \
    comboselectionutilitywidget.cpp \
    projectbuildsettingpagewidget.cpp \
    sourcefilebuildpredictor.cpp

HEADERS += armpluginqtcreatorplugin.h \
        armpluginqtcreator_global.h \
        armpluginqtcreatorconstants.h \
    armprojectmanager.h \
    armproject.h \
    armprojectnodes.h \
    includepathconfigurationwidget.h \
    armprojectbuildconfigurationfactory.h \
    armprojectgenericstep.h \
    armprojectwizardfactory.h \
    armprojectsettingdialog.h \
    ISettingPageWidget.h \
    includepathsettingpagewidget.h \
    symbolssettingpagewidget.h \
    Utils.h \
    armprojectbuildsetting.h \
    toolchaininfopage.h \
    comboselectionutilitywidget.h \
    projectbuildsettingpagewidget.h \
    sourcefilebuildpredictor.h

# Qt Creator linking

## set the QTC_SOURCE environment variable to override the setting here
QTCREATOR_SOURCES = $$(QTC_SOURCE)
isEmpty(QTCREATOR_SOURCES):QTCREATOR_SOURCES= ./../qt-creator

## set the QTC_BUILD environment variable to override the setting here
IDE_BUILD_TREE = $$(QTC_BUILD)
isEmpty(IDE_BUILD_TREE):IDE_BUILD_TREE=./../build-qtcreator-Desktop-Release

## uncomment to build plugin into user config directory
## <localappdata>/plugins/<ideversion>
##    where <localappdata> is e.g.
##    "%LOCALAPPDATA%\QtProject\qtcreator" on Windows Vista and later
##    "$XDG_DATA_HOME/data/QtProject/qtcreator" or "~/.local/share/data/QtProject/qtcreator" on Linux
##    "~/Library/Application Support/QtProject/Qt Creator" on Mac
# USE_USER_DESTDIR = yes

###### If the plugin can be depended upon by other plugins, this code needs to be outsourced to
###### <dirname>_dependencies.pri, where <dirname> is the name of the directory containing the
###### plugin's sources.

QTC_PLUGIN_NAME = ArmPluginQtCreator
QTC_LIB_DEPENDS += \
    # nothing here at this time

QTC_PLUGIN_DEPENDS += \
    coreplugin \
    projectexplorer \
    baremetal

QTC_PLUGIN_RECOMMENDS += \
    # optional plugin dependencies. nothing here at this time

###### End _dependencies.pri contents ######

include($$QTCREATOR_SOURCES/src/qtcreatorplugin.pri)

OTHER_FILES += \
    ArmProjectmimetype.xml \
    ThingsToLearn.txt

RESOURCES += \
    ArmPluginQtCreator.qrc

FORMS += \
    includepathconfigurationwidget.ui \
    armprojectsettingdialog.ui \
    toolchaininfopage.ui \
    comboselectionutilitywidget.ui \
    projectbuildsettingpage.ui

DEFINES += SANJAY
