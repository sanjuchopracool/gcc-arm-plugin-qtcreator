#include "sourcefilebuildpredictor.h"
#include <QFile>
#include <QTextStream>
#include <QFileInfo>
#include <QDebug>

namespace ArmPluginQtCreator {
namespace Internal {

SourceFileBuildPredictor::SourceFileBuildPredictor(const QString &compiler)
    : m_compilerCommand(compiler),
      m_process(0)
{

}

SourceFileBuildPredictor::~SourceFileBuildPredictor()
{
    if(m_process)
        m_process->terminate();

    m_process->deleteLater();
}

bool SourceFileBuildPredictor::needCompilation(const QString &sourceFilePath, const QDateTime &objFileLastModified) const
{
    m_process->setCommand(m_compilerCommand, m_arguments + QLatin1String(" -M " ) + sourceFilePath);
    m_process->start();
    m_process->waitForFinished();
    QByteArray error = m_process->readAllStandardError();
    if(!error.isEmpty())
        return true; //build it, because we can't comment on this

    QByteArray output = m_process->readAllStandardOutput(); //contains list of dependencies
    QStringList dependencies;
    QTextStream stream(&output, QIODevice::ReadOnly);
    QString str = stream.readLine(); //skip first line
    str = stream.readLine();
    while(!str.isEmpty())
    {
        str.remove(str.length() -1, 1 ); //remove last character
        if(!str.isEmpty())
            dependencies.append(str.trimmed());
        str = stream.readLine();
    }

    Q_FOREACH(const QString filePath ,dependencies)
    {
        if(QFileInfo(filePath).lastModified() > objFileLastModified)
            return true;
    }

    return false;
}

void SourceFileBuildPredictor::setIncludePaths(const QString &includePaths)
{
    m_arguments = includePaths;
}

void SourceFileBuildPredictor::setDefines(const QString &defines)
{
    m_arguments += QLatin1Char(' ') + defines;
}

void SourceFileBuildPredictor::setProcessParameters(const ProjectExplorer::ProcessParameters &param)
{
    m_param = param;
    // also check whether the command exists
    m_process = new Utils::QtcProcess;
    m_process->setWorkingDirectory(m_param.effectiveWorkingDirectory());
    m_process->setEnvironment(m_param.environment());
}


} // namespace Internal
} // namespace ArmProjectManager
