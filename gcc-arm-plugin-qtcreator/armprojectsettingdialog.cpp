#include "armprojectsettingdialog.h"
#include "ui_armprojectsettingdialog.h"
#include "includepathsettingpagewidget.h"
#include "symbolssettingpagewidget.h"
#include "toolchaininfopage.h"
#include "projectbuildsettingpagewidget.h"
#include <QDialogButtonBox>
#include <QPushButton>

namespace ArmPluginQtCreator {
namespace Internal {

SettingPageWidgetsModel::SettingPageWidgetsModel(QObject *parent):
    QAbstractListModel(parent)
{

}

SettingPageWidgetsModel::~SettingPageWidgetsModel()
{

}

QVariant SettingPageWidgetsModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(section)
    if(orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return tr("Setting page");

    return QVariant();
}

int SettingPageWidgetsModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_pages.count();
}

QVariant SettingPageWidgetsModel::data(const QModelIndex &index, int role) const
{
    int row = index.row();
    if(row < 0 || row >= m_pages.count())
        return QVariant();

    ISettingPageWidget* page = m_pages.at(row);
    if(role == Qt::DisplayRole)
        return page->name();

    if(role == Qt::DecorationRole)
        return page->icon();

    return QVariant();
}

void SettingPageWidgetsModel::addPage(ISettingPageWidget *page)
{
    //it will be done first
    m_pages.append(page);
}

ISettingPageWidget *SettingPageWidgetsModel::settingPage(int index) const
{
    if(index < m_pages.count())
        return m_pages.at(index);

    return 0;
}

const QList<ISettingPageWidget *> &SettingPageWidgetsModel::allSettingPages() const
{
    return m_pages;
}


////////////////////////////////////////////////////////////////////////////////
/// \brief ArmProjectSettingDialog::ArmProjectSettingDialog
/// \param project
/// \param parent
////////////////////////////////////////////////////////////////////////////////

ArmProjectSettingDialog::ArmProjectSettingDialog(ArmProjectBuildSetting& buildSetting, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ArmProjectSettingDialog),
    m_model(new SettingPageWidgetsModel(this))
{
    ui->setupUi(this);
    ui->listView->setIconSize(QSize(32, 32));
    ui->listView->setSelectionMode(QAbstractItemView::SingleSelection);
    setWindowTitle(tr("Project setting"));

    ui->listView->setModel(m_model);

    IncludePathSettingPageWidget* includeSettingPage = new IncludePathSettingPageWidget(buildSetting, this);
    addSettingPageWidget(includeSettingPage);

    SymbolsSettingPageWidget* symbolPage = new SymbolsSettingPageWidget(buildSetting, this);
    addSettingPageWidget(symbolPage);

    ToolChainInfoWidget* toolChainInfoWidget = new ToolChainInfoWidget(buildSetting, this);
    addSettingPageWidget(toolChainInfoWidget);

    ProjectBuildSettingPageWidget* projectBuildSetting = new ProjectBuildSettingPageWidget(buildSetting, this);
    addSettingPageWidget(projectBuildSetting);

    QDialogButtonBox* buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel | QDialogButtonBox::Apply,
                                                       Qt::Horizontal,this);
    ui->verticalLayout->addWidget(buttonBox);
    connect(ui->listView, SIGNAL(clicked(QModelIndex)), this, SLOT(slotCurrentIndexChanged(QModelIndex)));
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(slotAccept()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
    QPushButton* applyButton = buttonBox->button(QDialogButtonBox::Apply);
    connect(applyButton, SIGNAL(clicked()), this, SLOT(slotApply()));
    QModelIndex index = m_model->index(0, 0);
    ui->listView->setCurrentIndex(index);
    slotCurrentIndexChanged(index);
}

ArmProjectSettingDialog::~ArmProjectSettingDialog()
{
    delete ui;
}

void ArmProjectSettingDialog::addSettingPageWidget(ISettingPageWidget *pageWidget)
{
    m_model->addPage(pageWidget);
    ui->stackedWidget->addWidget(pageWidget->widget());
}

void ArmProjectSettingDialog::slotCurrentIndexChanged(QModelIndex index)
{
    ISettingPageWidget* page = m_model->settingPage(index.row());
    if(page)
        ui->stackedWidget->setCurrentWidget(page->widget());
}

void ArmProjectSettingDialog::slotAccept()
{
    Q_FOREACH(ISettingPageWidget* w, m_model->allSettingPages())
        w->apply();
    accept();
}

void ArmProjectSettingDialog::slotApply()
{
    ISettingPageWidget* currentPage = dynamic_cast<ISettingPageWidget*>(ui->stackedWidget->currentWidget());
    if(currentPage)
        currentPage->apply();
}

} // namespace Internal
} // namespace ArmProjectManager
