#include "armprojectgenericstep.h"
#include "armpluginqtcreatorconstants.h"
#include "armproject.h"
#include "Utils.h"

#include <projectexplorer/target.h>
#include <projectexplorer/task.h>
#include <projectexplorer/toolchain.h>
#include <projectexplorer/buildconfiguration.h>
#include <projectexplorer/gnumakeparser.h>
#include <projectexplorer/project.h>
#include <projectexplorer/buildsteplist.h>
#include <projectexplorer/projectexplorerconstants.h>
#include <QFutureInterface>
#include <QTimer>
#include <QDir>
#include <utils/qtcassert.h>
#include <QDateTime>
#include "sourcefilebuildpredictor.h"

using namespace ProjectExplorer;
namespace ArmPluginQtCreator {
namespace Internal {

const char ARM_PROJECT_GENERIC_STEP_ID[] = "ArmProjectManager.ArmProjectGenericStep";
const char ARM_PROJECT_GENERIC_CLEAN_STEP_KEY_ID[] = "ArmProjectManager.ArmProjectGenericStep.Clean";
const char GENERIC_MS_DISPLAY_NAME[] = QT_TRANSLATE_NOOP("ArmProjectManager::Internal::ArmProjectGenericStep",
                                                     "Build");

ArmProjectGenericBuildStep::ArmProjectGenericBuildStep(ProjectExplorer::BuildStepList *parent)
    : ProjectExplorer::BuildStep(parent, Core::Id(ARM_PROJECT_GENERIC_STEP_ID)),
      m_outputParserChain(0),
      m_process(0),
      m_timer(0),
      m_killProcess(false),
      m_cleanStep(false),
      m_toolChain(0),
      m_buildSetting(0)
{
    m_timer = new QTimer(this);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(checkForCancel()));
}

ArmProjectGenericBuildStep::~ArmProjectGenericBuildStep()
{
    if(m_outputParserChain)
        delete m_outputParserChain;
    if(m_process)
        delete m_process;
}

bool ArmProjectGenericBuildStep::init(QList<const BuildStep *> &earlierSteps)
{
    BuildConfiguration *bc = buildConfiguration();
    if (!bc)
        bc = target()->activeBuildConfiguration();
    if (!bc)
        emit addTask(Task::buildConfigurationMissingTask());

    ToolChain *tc = ToolChainKitInformation::toolChain(target()->kit(), ToolChain::Language::Cxx);
    if (!tc)
        emit addTask(Task::compilerMissingTask());

    if (!bc || !tc) {
        emitFaultyConfigurationMessage();
        return false;
    }

    m_param.setMacroExpander(bc->macroExpander());
    m_param.setWorkingDirectory(bc->buildDirectory().toString());
    Utils::Environment env = bc->environment();
    // Force output to english for the parsers. Do this here and not in the toolchain's
    // addToEnvironment() to not screw up the users run environment.
    env.set(QLatin1String("LC_ALL"), QLatin1String("C"));
    m_param.setEnvironment(env);
    m_param.resolveAll();

    IOutputParser *parser = target()->kit()->createOutputParser();
    if (parser)
        setOutputParser(parser);
    outputParser()->setWorkingDirectory(m_param.effectiveWorkingDirectory());
    return true;
}

void ArmProjectGenericBuildStep::run(QFutureInterface<bool> &fi)
{
    clearRunTimeVariables();
    if(!m_cleanStep)
        extractBuildInformationFromProject();
    m_futureInterface = &fi;
    m_process = new Utils::QtcProcess();
    m_process->setWorkingDirectory(m_param.effectiveWorkingDirectory());
    m_process->setEnvironment(m_param.environment());

    connect(m_process, SIGNAL(readyReadStandardOutput()),
            this, SLOT(processReadyReadStdOutput()));
    connect(m_process, SIGNAL(readyReadStandardError()),
            this, SLOT(processReadyReadStdError()));

    connect(m_process, SIGNAL(finished(int,QProcess::ExitStatus)),
            this, SLOT(slotProcessFinished(int,QProcess::ExitStatus)));

    if(m_cleanStep)
        startCleanStep();
    else
    {
        m_timer->start(500);
        if(!m_sourceFileToCompileCount)
            startLinkingStep();
        else
            startNextSourceCompilationProcess();
    }
}

ProjectExplorer::BuildStepConfigWidget *ArmProjectGenericBuildStep::createConfigWidget()
{
    return new ReadOnlyGenericStepWidget(this);
}


QVariantMap ArmProjectGenericBuildStep::toMap() const
{
    QVariantMap map(BuildStep::toMap());
    map.insert(QLatin1String(ARM_PROJECT_GENERIC_CLEAN_STEP_KEY_ID), m_cleanStep);
    return map;
}

void ArmProjectGenericBuildStep::setOutputParser(ProjectExplorer::IOutputParser *parser)
{
    delete m_outputParserChain;
    //m_outputParserChain = new AnsiFilterParser;
    m_outputParserChain = parser;

    if (m_outputParserChain) {
        connect(m_outputParserChain, SIGNAL(addOutput(QString,ProjectExplorer::BuildStep::OutputFormat)),
                this, SLOT(outputAdded(QString,ProjectExplorer::BuildStep::OutputFormat)));
        connect(m_outputParserChain, SIGNAL(addTask(ProjectExplorer::Task)),
                this, SLOT(taskAdded(ProjectExplorer::Task)));
    }
}

void ArmProjectGenericBuildStep::appendOutputParser(ProjectExplorer::IOutputParser *parser)
{
    if (!parser)
        return;

    QTC_ASSERT(m_outputParserChain, return);
    m_outputParserChain->appendOutputParser(parser);
    return;
}

ProjectExplorer::IOutputParser *ArmProjectGenericBuildStep::outputParser() const
{
    return m_outputParserChain;
}

bool ArmProjectGenericBuildStep::runInGuiThread() const
{
    return true;
}

void ArmProjectGenericBuildStep::setClean(bool clean)
{
    m_cleanStep = clean;
}

bool ArmProjectGenericBuildStep::isCleanStep() const
{
    return m_cleanStep;
}

bool ArmProjectGenericBuildStep::fromMap(const QVariantMap &map)
{
    m_cleanStep = map.value(QLatin1String(ARM_PROJECT_GENERIC_CLEAN_STEP_KEY_ID)).toBool();
    return BuildStep::fromMap(map);
}

ArmProjectGenericBuildStep::ArmProjectGenericBuildStep(BuildStepList *parent, ArmProjectGenericBuildStep *bs)
    : ProjectExplorer::BuildStep(parent, Core::Id(ARM_PROJECT_GENERIC_STEP_ID)),
      m_param(bs->m_param),
      m_outputParserChain(0),
      m_process(0),
      m_timer(0),
      m_killProcess(false),
      m_cleanStep(bs->m_cleanStep),
      m_toolChain(0)
{
    Q_UNUSED(bs)
    m_timer = new QTimer(this);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(checkForCancel()));
}

void ArmProjectGenericBuildStep::emitFaultyConfigurationMessage()
{
    emit addOutput(tr("Configuration is faulty. Check the Issues view for details."),
                   BuildStep::MessageOutput);
}

void ArmProjectGenericBuildStep::cleanUp(bool result)
{
    // Clean up output parsers
    if (m_outputParserChain) {
        delete m_outputParserChain;
        m_outputParserChain = 0;
    }

    delete m_process;
    m_process = 0;
    m_futureInterface->reportResult(result);
    m_futureInterface->reportFinished();
    m_futureInterface = 0;
}

void ArmProjectGenericBuildStep::extractBuildInformationFromProject()
{
    QDir wd(m_param.effectiveWorkingDirectory());
    if (!wd.exists())
        wd.mkpath(wd.absolutePath());

    m_project = qobject_cast<ArmProject*>(this->buildConfiguration()->target()->project());
    if(m_project)
    {
        m_buildSetting = &m_project->projectBuildSetting();
        Q_FOREACH(const QString path, m_project->allIncludePaths())
            m_includepaths += QLatin1String(" -I") + path;

        Q_FOREACH(const QString define, m_buildSetting->symbols())
            m_defines += QLatin1String(" -D") + define;

        const ArmToolChain& toolchain = m_buildSetting->toolChain();
        m_cppCompilerCommand = toolchain.cppCompiler();
        m_cCompilerCommand = toolchain.cCompiler();
        m_objcopyCommand = toolchain.objcopy();

        SourceFileBuildPredictor fileBuildPredictor(m_cCompilerCommand);
        fileBuildPredictor.setIncludePaths(m_includepaths);
        fileBuildPredictor.setDefines(m_defines);
        fileBuildPredictor.setProcessParameters(m_param);
        Q_FOREACH(const QString& filePath, m_project->sourceFiles())
        {
            QFileInfo fileInfo(filePath);
            if(!fileInfo.exists())
            {
                stdError(QString(QLatin1String("%1 does not exist!\n")).arg(filePath));
                continue;
            }

            QString objectFile = fileInfo.baseName() + QLatin1String(".o");
            if(wd.exists(objectFile))
            {
                QFileInfo objectFileInfo(wd.filePath(objectFile));
                QDateTime lastModificationTime = objectFileInfo.lastModified();
                if( lastModificationTime > fileInfo.lastModified() &&
                        !fileBuildPredictor.needCompilation(filePath, lastModificationTime))
                {
                    stdOutput(QString(QLatin1String("object file for %1 is already upto date\n")).arg(filePath));
                    continue;
                }
            }
            m_filesHash.insert(filePath, objectFile);
            stdOutput(QString(QLatin1String("%1 added for compilation.\n")).arg(filePath));
        }
    }
    m_sourceFileToCompileCount = m_filesHash.count();
}

void ArmProjectGenericBuildStep::processReadyReadStdOutput()
{
    m_process->setReadChannel(QProcess::StandardOutput);
    while (m_process->canReadLine()) {
        QString line = QString::fromLocal8Bit(m_process->readLine());
        stdOutput(line);
    }
}

void ArmProjectGenericBuildStep::processReadyReadStdError()
{
    m_process->setReadChannel(QProcess::StandardError);
    while (m_process->canReadLine()) {
        QString line = QString::fromLocal8Bit(m_process->readLine());
        stdError(line);
    }
}

void ArmProjectGenericBuildStep::slotProcessFinished(int, QProcess::ExitStatus)
{
    QString line = QString::fromLocal8Bit(m_process->readAllStandardError());
    if (!line.isEmpty())
        stdError(line);

    line = QString::fromLocal8Bit(m_process->readAllStandardOutput());
    if (!line.isEmpty())
        stdOutput(line);

    processFinished(m_process->exitCode(), m_process->exitStatus());
    bool success = (m_process->exitCode() == 0 &&  m_process->exitStatus() == QProcess::NormalExit);
    if(success && !m_cleanStep)
    {
        if(m_CompiledFileIndex < m_sourceFileToCompileCount)
            startNextSourceCompilationProcess();
        else if(m_afterCompileStatus == 0)
            startLinkingStep(); //start linking
        else if(m_afterCompileStatus < 3)
            startObjcopyProcess();
        else
        {
            if(m_project)
                m_project->updateApplicationTargets();

            m_timer->stop();
            cleanUp(success);
        }
    }
    else
        cleanUp(success);
}

void ArmProjectGenericBuildStep::outputAdded(const QString &string, BuildStep::OutputFormat format)
{
    emit addOutput(string, format, BuildStep::DontAppendNewline);
}

void ArmProjectGenericBuildStep::stdOutput(const QString &line)
{
    if (m_outputParserChain)
        m_outputParserChain->stdOutput(line);
    emit addOutput(line, BuildStep::NormalOutput, BuildStep::DontAppendNewline);
}

void ArmProjectGenericBuildStep::stdError(const QString &line)
{
    if (m_outputParserChain)
        m_outputParserChain->stdError(line);
    emit addOutput(line, BuildStep::ErrorOutput, BuildStep::DontAppendNewline);
}

void ArmProjectGenericBuildStep::checkForCancel()
{
    if(!m_futureInterface)
    {
        m_timer->stop();
        return;
    }

    if (m_futureInterface->isCanceled() && m_timer->isActive())
    {
        if (!m_killProcess)
        {
            m_process->terminate();
            m_killProcess = true;
        }
        else
        {
            m_process->kill();
            m_timer->stop();
        }
    }
}

void ArmProjectGenericBuildStep::taskAdded(const Task &task)
{

    if(m_outputParserChain)
        m_outputParserChain->flush();

    Task editable(task);
    QString filePath = task.file.toString();
    if (!filePath.isEmpty() && !QDir::isAbsolutePath(filePath)) {
        // We have no save way to decide which file in which subfolder
        // is meant. Therefore we apply following heuristics:
        // 1. Check if file is unique in whole project
        // 2. Otherwise try again without any ../
        // 3. give up.

        QList<QFileInfo> possibleFiles;
        QString fileName = QFileInfo(filePath).fileName();
        foreach (const QString &file, project()->files(ProjectExplorer::Project::AllFiles)) {
            QFileInfo candidate(file);
            if (candidate.fileName() == fileName)
                possibleFiles << candidate;
        }

        if (possibleFiles.count() == 1) {
            editable.file = Utils::FileName(possibleFiles.first());
        } else {
            // More then one filename, so do a better compare
            // Chop of any "../"
            while (filePath.startsWith(QLatin1String("../")))
                filePath.remove(0, 3);
            int count = 0;
            QString possibleFilePath;
            foreach (const QFileInfo &fi, possibleFiles) {
                if (fi.filePath().endsWith(filePath)) {
                    possibleFilePath = fi.filePath();
                    ++count;
                }
            }
            if (count == 1)
                editable.file = Utils::FileName::fromString(possibleFilePath);
            else
                qWarning() << "Could not find absolute location of file " << filePath;
        }
    }
    emit addTask(editable);
}

void ArmProjectGenericBuildStep::startNextSourceCompilationProcess()
{
    QList<QString> filesToCompile = m_filesHash.keys();
    if(m_CompiledFileIndex < filesToCompile.count())
    {
        QString fileToCompile = filesToCompile.at(m_CompiledFileIndex++);
        FileExtensionType type = extensionType(fileToCompile);
        const CCompilationSetting& cSetting = m_buildSetting->cCompilationSetting();
        const AssemblerSetting& assemblerSetting = m_buildSetting->assemblerSetting();
        const ArmTargetSetting& targetSetting = m_buildSetting->targetCpuSetting();
        QString command;
        QString flags;
        switch (type)
        {
        case CFILE:
            command = m_cCompilerCommand;
            flags = cSetting.cFlags();
            break;
        case CPPFILE:
            command = m_cppCompilerCommand;
            flags = cSetting.cppFlags();
            m_haveCppFiles = true;
            break;
        case ASFILE:
            command = m_cCompilerCommand;
            flags = assemblerSetting.flags();
            break;
        default:
            return; //only source file can be there otherwise return
        }
        QStringList argStringList;
        argStringList.append(QLatin1String(" -c "));
        argStringList.append(targetSetting.flags());
        if(type != ASFILE)//in case of Assembly no need to include and define
        {
            argStringList.append(m_defines);
            argStringList.append(m_includepaths);
        }

        argStringList.append(flags);
        if(type == ASFILE)
            argStringList.append(QLatin1String(" -o ") + m_filesHash.value(fileToCompile));
        argStringList.append(fileToCompile);
        m_param.setCommand(command);
        m_param.setArguments(argStringList.join(QLatin1Char(' ')));
        startProcessFromInternalParams();
    }
}

void ArmProjectGenericBuildStep::startCleanStep()
{
    m_param.setCommand(QLatin1String("rm"));
    m_param.setArguments(QLatin1String(" -rf *.o *.elf *.bin *.hex"));
    startProcessFromInternalParams();
}

void ArmProjectGenericBuildStep::startLinkingStep()
{
    if(!m_buildSetting->linkerSetting().linkerFile.isEmpty())
    {
        const OtherSetting& otherSetting = m_buildSetting->otherSetting();
        if(otherSetting.createBin)
            m_afterCompileStatus = 1;//bin
        else if(otherSetting.createHex)
            m_afterCompileStatus = 2;//hex
        else
            m_afterCompileStatus = 3;//exit

        if(m_haveCppFiles)
            m_param.setCommand(m_cppCompilerCommand);//use G++ for linking in case we have any cpp file
        else
            m_param.setCommand(m_cCompilerCommand);

        QStringList arguments;
        arguments.append(QString(QLatin1Literal(" *.o  -o %1.elf ")).arg(m_project->displayName().trimmed()));
        arguments.append(m_buildSetting->targetCpuSetting().flags());
        arguments.append(m_buildSetting->linkerSetting().flags());
        m_param.setArguments(arguments.join(QLatin1Char(' ')));
        startProcessFromInternalParams();
    }
    else
    {
        stdError(tr("error: no valid linker file to link\n"));
        cleanProcessOnFailure();
    }
}

void ArmProjectGenericBuildStep::clearRunTimeVariables()
{
    m_filesHash.clear();
    m_includepaths.clear();
    m_CompiledFileIndex = 0;
    m_defines.clear();
    m_afterCompileStatus = 0;
    m_sourceFileToCompileCount = 0;
    m_project = 0;
    m_haveCppFiles = false;
}

void ArmProjectGenericBuildStep::startObjcopyProcess()
{
    //first create bin and then hex
    m_param.setCommand(m_objcopyCommand);
    QString projectName = project()->displayName();
    projectName = projectName.trimmed();
    QString argument = QLatin1String(" -O ");
    if(m_afterCompileStatus == 1)
    {
        const OtherSetting& otherSetting = m_buildSetting->otherSetting();
        argument += QString(QLatin1String(" binary  %1.elf %1.bin")).arg(projectName);
        if(otherSetting.createHex)
            m_afterCompileStatus = 2;
        else
            m_afterCompileStatus = 3;
    }
    else
    {
        argument += QString(QLatin1String(" ihex %1.elf %1.hex")).arg(projectName);
        m_afterCompileStatus = 3;
    }
    m_param.setArguments(argument);
    startProcessFromInternalParams();
}

void ArmProjectGenericBuildStep::startProcessFromInternalParams()
{
    processStarted();
    m_process->setCommand(m_param.command(), m_param.arguments());
    m_process->start();
    if (!m_process->waitForStarted())
        cleanProcessOnFailure();
}

void ArmProjectGenericBuildStep::cleanProcessOnFailure()
{
    processStartupFailed();
    delete m_process;
    m_process = 0;
    m_futureInterface->reportResult(false);
    m_futureInterface->reportFinished();
    return;
}
void ArmProjectGenericBuildStep::processStarted()
{
    emit addOutput(tr("Starting: \"%1\" %2\n")
                   .arg(QDir::toNativeSeparators(m_param.effectiveCommand()),
                        m_param.prettyArguments()),
                   BuildStep::MessageOutput);
}

/*!
    Called after the process is finished.

    The default implementation adds a line to the output window.
*/

void ArmProjectGenericBuildStep::processFinished(int exitCode, QProcess::ExitStatus status)
{
    if (m_outputParserChain)
        m_outputParserChain->flush();

    QString command = QDir::toNativeSeparators(m_param.effectiveCommand());
    if (status == QProcess::NormalExit && exitCode == 0) {
        emit addOutput(tr("The process \"%1\" exited normally\n.").arg(command),
                       BuildStep::MessageOutput);
    } else if (status == QProcess::NormalExit) {
        emit addOutput(tr("The process \"%1\" exited with code %2.\n")
                       .arg(command, QString::number(m_process->exitCode())),
                       BuildStep::ErrorMessageOutput);
    } else {
        emit addOutput(tr("The process \"%1\" crashed.").arg(command), BuildStep::ErrorMessageOutput);
    }
}

/*!
    Called if the process could not be started.

    By default, adds a message to the output window.
*/

void ArmProjectGenericBuildStep::processStartupFailed()
{
    emit addOutput(tr("Could not start process \"%1\" %2\n")
                   .arg(QDir::toNativeSeparators(m_param.effectiveCommand()),
                        m_param.prettyArguments()),
                   BuildStep::ErrorMessageOutput);
}

/*!
    Called to test whether a process succeeded or not.
*/

bool ArmProjectGenericBuildStep::processSucceeded(int exitCode, QProcess::ExitStatus status)
{
    if (outputParser() && outputParser()->hasFatalErrors())
        return false;

    return exitCode == 0 && status == QProcess::NormalExit;
}

//
// MakeStepFactory
//

ArmProjectBuildStepFactory::ArmProjectBuildStepFactory(QObject *parent) :
    IBuildStepFactory(parent)
{
}


BuildStep *ArmProjectBuildStepFactory::create(BuildStepList *parent, const Core::Id id)
{
    ArmProjectGenericBuildStep *step = new ArmProjectGenericBuildStep(parent);
    if (parent->id() == ProjectExplorer::Constants::BUILDSTEPS_CLEAN)
        step->setClean(true);
    return step;
}

BuildStep *ArmProjectBuildStepFactory::clone(BuildStepList *parent, BuildStep *source)
{
    ArmProjectGenericBuildStep *old(qobject_cast<ArmProjectGenericBuildStep *>(source));
    Q_ASSERT(old);
    return new ArmProjectGenericBuildStep(parent, old);
}

QList<BuildStepInfo> ArmProjectBuildStepFactory::availableSteps(BuildStepList *parent) const
{
    if (parent->target()->project()->id() != Constants::ARM_PROJECT_ID)
        return {};

    return {{ ARM_PROJECT_GENERIC_STEP_ID,
              QCoreApplication::translate("GenericProjectManager::Internal::GenericMakeStep",
              GENERIC_MS_DISPLAY_NAME) }};
}

} // namespace Internal
} // namespace ArmProjectManager
