#ifndef COMBOSELECTIONUTILITYWIDGET_H
#define COMBOSELECTIONUTILITYWIDGET_H

#include <QWidget>

namespace Ui {
class ComboSelectionUtilityWidget;
}

namespace ArmPluginQtCreator {
namespace Internal {

class ComboSelectionUtilityWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ComboSelectionUtilityWidget(QWidget *parent = 0);
    ~ComboSelectionUtilityWidget();

    void init(const QString& name, const QList< QPair<QString, QString> >& items, int currentIndex);
    QString value() const;
    void setCurrentValue(QString userData);

private slots:
    void on_comboBox_currentIndexChanged(const QString &arg1);

signals:
    void valueChanged();

private:
    Ui::ComboSelectionUtilityWidget *ui;
};


} // namespace Internal
} // namespace ArmPluginQtCreator

#endif // COMBOSELECTIONUTILITYWIDGET_H
