#ifndef INCLUDEPATHSETTINGPAGEWIDGET_H
#define INCLUDEPATHSETTINGPAGEWIDGET_H

#include "includepathconfigurationwidget.h"
#include "ISettingPageWidget.h"
#include "armprojectbuildsetting.h"

namespace ArmPluginQtCreator {
namespace Internal {

class ArmProject;
class IncludePathSettingPageWidget : public StringListManagementWidget,
        virtual public ISettingPageWidget
{
public:
    IncludePathSettingPageWidget(ArmProjectBuildSetting& buildSetting, QWidget* parent = 0);
    ~IncludePathSettingPageWidget();

    QString name() const;
    const QIcon& icon() const;
    QWidget* widget();
    void apply();

private:
    ArmProjectBuildSetting& m_buildSetting;
    QIcon m_icon;
};


} // namespace Internal
} // namespace ArmProjectManager

#endif // INCLUDEPATHSETTINGPAGEWIDGET_H
