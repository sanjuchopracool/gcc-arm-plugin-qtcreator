TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG += qt
QT += xml
DEFINES += PROJECT_IMPORTER
SOURCES += main.cpp \
    ../armprojectbuildsetting.cpp

HEADERS += \
    ../armprojectbuildsetting.h

#find . -name ".project" | grep -i truestudio > allProjects.txt   #This is the command to be used
