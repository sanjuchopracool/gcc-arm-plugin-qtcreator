#include <iostream>
#include <QDebug>
#include <QDomDocument>
#include <QDomElement>
#include <QFile>
#include <QStringList>
#include <QDir>
#include <QFileInfo>
#include <QTextStream>
#include "../armprojectbuildsetting.h"

using namespace std;
using namespace ArmPluginQtCreator::Internal;
QStringList relativeFilePaths(const QDir& dir,const QStringList filePaths)
{
    QStringList relativePaths;
    Q_FOREACH(const QString& path, filePaths)
        relativePaths.append(dir.relativeFilePath(path));
    return relativePaths;
}

int main()
{
    QFile atollicProjectsFileListing("/home/sanju/STM32Cube/STM32Cube_FW_F3_V1.2.0/allProjects.txt");
    if(atollicProjectsFileListing.open(QIODevice::ReadOnly))
    {
        QDir chopsProjectDirectory("/home/sanju/STM32Cube/STM32Cube_FW_F3_V1.2.0/chopsProjects/");
        QDir globalProjectDirectory("/home/sanju/STM32Cube/STM32Cube_FW_F3_V1.2.0/Projects/");
        QTextStream stream(&atollicProjectsFileListing);
        QHash<QString, QString> allProjectFiles;
        QString filePath = stream.readLine();
        while(!filePath.isEmpty())
        {
            allProjectFiles.insert(filePath, QString());
            filePath = stream.readLine();
        }

        // now we have list of all project files
        int importedProjectCount = 0;
        Q_FOREACH(const QString& filePath, allProjectFiles.keys())
        {
            QFileInfo fileInfo(filePath);
            QDir projectDir = fileInfo.absoluteDir();
            projectDir.cdUp();
            projectDir.cdUp();
            // project dir is the dir
            //            qDebug() << projectDir.absolutePath();
            //            qDebug() << globalProjectDirectory.absolutePath();
            QString relativePathtoCreate = projectDir.absolutePath().remove(globalProjectDirectory.absolutePath()).remove(0,1);
            QString absolutePathToCreate = chopsProjectDirectory.absolutePath() + "/" + relativePathtoCreate;
            QString projectName = QDir(absolutePathToCreate).dirName();
            QString absoluteProjectFileToCreate = absolutePathToCreate + "/" + projectName + ".chops";
            qDebug() << projectDir.mkpath(absolutePathToCreate) << absoluteProjectFileToCreate;
            allProjectFiles[filePath] = absoluteProjectFileToCreate;
            QDomDocument doc;
            QFile file(filePath);
            QFile CFile(fileInfo.absoluteDir().absoluteFilePath(".cproject"));
            QStringList projectFiles;
            QStringList includesPaths;
            QStringList ProjectDefines;
            ArmProjectBuildSetting buildSetting;
            if (file.open(QIODevice::ReadOnly))
            {
                QDomDocument doc;
                QDir fileDir = QFileInfo(filePath).absoluteDir();
                if (doc.setContent(&file))
                {
                    QDomElement projectDescription = doc.documentElement();
                    QDomElement nameElement = projectDescription.firstChildElement("name");
                    qDebug() << "nameElement text: " << nameElement.text();
                    QDomElement linkedResourceElement = projectDescription.firstChildElement("linkedResources");
                    QDomNode linkNode = linkedResourceElement.firstChildElement("link");
                    QString fileType("1");
                    while (!linkNode.isNull())
                    {
                        QDomElement linkElement = linkNode.toElement();
                        if (!linkElement.isNull())
                        {
                            // get type of link element
                            QDomElement typeElement = linkElement.firstChildElement("type");
                            if (!typeElement.isNull())
                            {
                                if (typeElement.text().trimmed() == fileType)
                                {
                                    QString linkFilePath = linkElement.firstChildElement("locationURI").text().trimmed();
                                    if (!linkFilePath.isEmpty())
                                    {
                                        int indexOfSlash = linkFilePath.indexOf("/");
                                        if (indexOfSlash >= 0)
                                        {
                                            QString startPath = linkFilePath.left(indexOfSlash);
                                            QStringList splitString = startPath.split("-");
                                            if (splitString.count() == 3)
                                            {
                                                bool ok = false;
                                                int upIndex = splitString.at(1).toInt(&ok);
                                                if (ok)
                                                {
                                                    QString endPath = linkFilePath.right(linkFilePath.length() - indexOfSlash);
                                                    QDir dir = fileDir;
                                                    while (upIndex)
                                                    {
                                                        dir.cdUp();
                                                        upIndex--;
                                                    }
                                                    projectFiles.append(dir.absolutePath() + endPath);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        linkNode = linkNode.nextSibling();
                    }
                }
            }

            ArmTargetSetting targetSetting;
            CCompilationSetting cCompilerSetting;
            LinkerSetting linkerSetting;
            //parse CFile
            if (CFile.open(QIODevice::ReadOnly))
            {
                QDir cFileDir = fileInfo.absoluteDir();
                if(doc.setContent(CFile.readAll()))
                {
                    QDomElement docE = doc.documentElement();
                    QDomElement p1E = docE.firstChildElement("storageModule");
                    QDomElement p2E = p1E.firstChildElement("cconfiguration");
                    QDomElement p3E = p2E.firstChildElement("storageModule");
                    p3E = p3E.nextSiblingElement(); //second storage
                    QDomElement p4E = p3E.firstChildElement("configuration");
                    QDomElement p5E = p4E.firstChildElement("folderInfo");
                    QDomElement p6E = p5E.firstChildElement("toolChain");
                    QDomElement p7E = p6E.firstChildElement("tool");//LINKER SETTING read fpu

                    QDomElement optionE = p7E.firstChildElement("option");
                    while(!optionE.isNull())
                    {
                        QString name = optionE.attribute("name");
                        if( name == "Floating point")
                            targetSetting.floatAbi = optionE.attribute("value").split('.').last();
                        else if(name == "FPU")
                            targetSetting.fpu = optionE.attribute("value").toLower();

                        optionE = optionE.nextSiblingElement();
                    }
                    p7E = p7E.nextSiblingElement();
                    QDomElement p8E = p7E.firstChildElement("option");
                    QDomElement listValueE = p8E.firstChildElement("listOptionValue");
                    while(!listValueE.isNull())
                    {
                        QString nativeRelative = listValueE.attribute("value").replace('\\', '/');
                        includesPaths << QDir::cleanPath(cFileDir.absoluteFilePath(nativeRelative.remove(0,3)));
                        listValueE = listValueE.nextSiblingElement();
                    }
                    buildSetting.setUserIncludePaths(includesPaths);

                    p8E = p8E.nextSiblingElement();
                    QDomElement symbolDefineE = p8E.firstChildElement("listOptionValue");
                    while(!symbolDefineE.isNull())
                    {
                        ProjectDefines.append(symbolDefineE.attribute("value"));
                        symbolDefineE = symbolDefineE.nextSiblingElement();
                    }
                    buildSetting.setSymbols(ProjectDefines);

                    while(!p8E.isNull())
                    {
                        QString name = p8E.attribute("name");
                        if( name == "Other options")
                            cCompilerSetting.extraFlags = optionE.attribute("value");
                        p8E = p8E.nextSiblingElement();
                    }

                    p7E = p7E.nextSiblingElement(); //linker
                    QDomElement option = p7E.firstChildElement("option");
                    QStringList libs;
                    QStringList LibPaths;
                    while(!option.isNull())
                    {
                        QString name = option.attribute("name");
                        name = name.trimmed();
                        if(name == "Linker script")
                        {
                            QString nativeRelative = option.attribute("value").replace('\\', '/');
                            linkerSetting.linkerFile = fileInfo.dir().absoluteFilePath(QFileInfo(nativeRelative).fileName());
                            //                    qDebug() << setting.linkerFile;
                            projectFiles << linkerSetting.linkerFile;
                        }
                        if(name == "Dead code removal")
                        {
                            linkerSetting.removeDeadCode = (option.attribute("value").trimmed() == "true");
                            qDebug() << "Dead Code: "<< option.attribute("value").trimmed() << linkerSetting.removeDeadCode;
                        }
                        if(name == "Libraries")
                        {
                            QDomElement listOptionE = option.firstChildElement("listOptionValue");
                            while(!listOptionE.isNull())
                            {
                                libs.append(listOptionE.attribute("value").remove(0, 1));
                                listOptionE = listOptionE.nextSiblingElement();
                            }
                        }
                        if(name == "Library search path")
                        {
                            QDomElement listOptionE = option.firstChildElement("listOptionValue");
                            while(!listOptionE.isNull())
                            {
                                QString nativeRelative = listOptionE.attribute("value").replace('\\', '/');
                                nativeRelative.remove(0, 3);
                                QDir dir = fileInfo.dir();
                                LibPaths.append(QDir::cleanPath(dir.absoluteFilePath(nativeRelative)));
                                listOptionE = listOptionE.nextSiblingElement();
                            }
                        }
                        option = option.nextSiblingElement();
                    }

                    Q_FOREACH(const QString& lib, libs)
                    {
                        Q_FOREACH(const QString& dirPath, LibPaths)
                        {
                            QDir dir(dirPath);
                            if(dir.exists(lib))
                            {
                                linkerSetting.staticLibraries.append(dir.absoluteFilePath(lib));
                            }
                        }

                    }
                }
            }

            //            qDebug() << "include paths " << includesPaths << "\n";
            //save project
            buildSetting.setTargetSetting(targetSetting);
            buildSetting.setCCompilationSetting(cCompilerSetting);
            buildSetting.setLinkerSetting(linkerSetting);
            QString projectFilePath = absoluteProjectFileToCreate;
            QDir chopProjectDir = QFileInfo(projectFilePath).absoluteDir();
            buildSetting.setRefrenceDirectory(chopProjectDir.absolutePath());
            QFile projectFile(projectFilePath);
            if(projectFile.open(QIODevice::WriteOnly | QIODevice::Truncate))
            {
                QDomDocument doc;
                QDomElement rootE = doc.createElement(QLatin1String("ChopsProject"));
                doc.appendChild(rootE);

                QDomElement includedFilesE = doc.createElement(QLatin1String("Files"));
                QStringList relativePaths = relativeFilePaths(chopProjectDir, projectFiles);
                Q_FOREACH(const QString& path, relativePaths)
                {
                    QDomElement fileE = doc.createElement(QLatin1String("file"));
                    fileE.appendChild(doc.createTextNode(path));
                    includedFilesE.appendChild(fileE);
                }
                rootE.appendChild(includedFilesE);

                buildSetting.save(rootE, doc);
                projectFile.write(doc.toByteArray());

                importedProjectCount++;
                QString msg = QString("Successfully imported %1 projects").arg(importedProjectCount);
                qDebug() << msg;
            }
        }

    }
    return 0;
}

