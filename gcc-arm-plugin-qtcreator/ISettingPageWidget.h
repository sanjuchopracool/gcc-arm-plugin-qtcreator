#ifndef SETTINGPAGEWIDGETINTERFACE_H
#define SETTINGPAGEWIDGETINTERFACE_H
#include <QString>
#include <QIcon>

class QWidget;

namespace ArmPluginQtCreator {
namespace Internal {

class ISettingPageWidget
{
public:
    ISettingPageWidget() {

    }

    ~ISettingPageWidget() {

    }

    ISettingPageWidget(const QString& name, const QString& iconPath)
        : m_name(name),
          m_icon(QIcon(iconPath))
    {

    }

    virtual QString name() const {
        return m_name;
    }
    virtual const QIcon& icon() const {
        return m_icon;
    }
    virtual QWidget* widget() = 0;
    virtual void apply() = 0;

protected:
    virtual void setName(const QString& name) {
        m_name = name;
    }

    virtual void setIconPath(const QString& name) {
        m_icon = QIcon(name);
    }

private:
    QString m_name;
    QIcon m_icon;
};

} // namespace Internal
} // namespace ArmProjectManager

#endif // SETTINGPAGEWIDGETINTERFACE_H
