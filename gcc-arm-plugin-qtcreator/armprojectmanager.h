#ifndef ARMPROJECTMANAGER_H
#define ARMPROJECTMANAGER_H

#include <projectexplorer/iprojectmanager.h>

namespace ArmPluginQtCreator {
namespace Internal {

class ArmProject;

class ArmProjectManager : public ProjectExplorer::IProjectManager
{
    Q_OBJECT

public:
    explicit ArmProjectManager();

    QString mimeType() const;
    // fileName is a canonical path!
    ProjectExplorer::Project *openProject(const QString &fileName, QString *errorString);
    void registerProject(ArmProject *project);
    void unregisterProject(ArmProject *project);

private:
    QList<ArmProject* > m_projects;
};

} // namespace Internal
} // namespace  ArmProjectManager

#endif // ARMPROJECTMANAGER_H
