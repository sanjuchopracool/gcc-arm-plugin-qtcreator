#ifndef ARMPROJECTBUILDCONFIGURATIONFACTORY_H
#define ARMPROJECTBUILDCONFIGURATIONFACTORY_H

#include <QObject>
#include <projectexplorer/buildconfiguration.h>
#include <projectexplorer/namedwidget.h>
#include <utils/pathchooser.h>
#include <projectexplorer/buildstep.h>

namespace ArmPluginQtCreator {
namespace Internal {

class ArmProjectBuildConfigurationFactory;
class ArmProjectBuildConfiguration : public ProjectExplorer::BuildConfiguration
{
    Q_OBJECT
    friend class ArmProjectBuildConfigurationFactory;
    friend class ArmProjectBuildSettingsWidget;
public:
    explicit ArmProjectBuildConfiguration(ProjectExplorer::Target *parent);
    ~ArmProjectBuildConfiguration();

    ProjectExplorer::NamedWidget *createConfigWidget();
    BuildType buildType() const;

protected:
    ArmProjectBuildConfiguration(ProjectExplorer::Target *parent, ArmProjectBuildConfiguration *source);
    ArmProjectBuildConfiguration(ProjectExplorer::Target *parent, Core::Id id);
};

class ArmProjectBuildConfigurationFactory : public ProjectExplorer::IBuildConfigurationFactory
{
    Q_OBJECT
public:
    explicit ArmProjectBuildConfigurationFactory(QObject *parent = 0);
    ~ArmProjectBuildConfigurationFactory();

    int priority(const ProjectExplorer::Target *t) const;
    int priority(const ProjectExplorer::Kit *k, const QString &projectPath) const;

    QList<ProjectExplorer::BuildInfo *> availableBuilds(const ProjectExplorer::Target *parent) const;
    QList<ProjectExplorer::BuildInfo *> availableSetups(const ProjectExplorer::Kit *k, const QString &projectPath) const;

    ProjectExplorer::BuildConfiguration *create(ProjectExplorer::Target *parent, const ProjectExplorer::BuildInfo *info) const;

    // used to recreate the runConfigurations when restoring settings
    bool canRestore(const ProjectExplorer::Target *parent, const QVariantMap &map) const;
    ProjectExplorer::BuildConfiguration *restore(ProjectExplorer::Target *parent, const QVariantMap &map);
    bool canClone(const ProjectExplorer::Target *parent, ProjectExplorer::BuildConfiguration *source) const;
    ProjectExplorer::BuildConfiguration *clone(ProjectExplorer::Target *parent, ProjectExplorer::BuildConfiguration *source);

private:
    bool canHandle(const ProjectExplorer::Target *t) const;
    ProjectExplorer::BuildInfo *createBuildInfo(const ProjectExplorer::Kit *k, const Utils::FileName &buildDir) const;
};

class ArmProjectBuildSettingsWidget : public ProjectExplorer::NamedWidget
{
    Q_OBJECT

public:
    ArmProjectBuildSettingsWidget(ArmProjectBuildConfiguration *bc);

private slots:
    void buildDirectoryChanged();
    void environmentHasChanged();

private:
    Utils::PathChooser *m_pathChooser;
    ArmProjectBuildConfiguration *m_buildConfiguration;
};

} // namespace Internal
} // namespace ArmProjectManager

#endif // ARMPROJECTBUILDCONFIGURATIONFACTORY_H
