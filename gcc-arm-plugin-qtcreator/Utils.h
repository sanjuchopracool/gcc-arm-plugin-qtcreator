#ifndef UTILS_H
#define UTILS_H

#include <QString>
#include <QFileInfo>
#include <projectexplorer/kitinformation.h>
#include <baremetal/baremetalconstants.h>

namespace ArmPluginQtCreator {
namespace Internal {

enum FileExtensionType
{
    HEADERFILE,
    CFILE,
    CPPFILE,
    ASFILE,
    LDFILE,
    OTHER
};

static FileExtensionType extensionType(const QString& filePath)
{
    QString suffix = QFileInfo(filePath).suffix();
    if(suffix == QLatin1String("c"))
        return CFILE;

    if(suffix == QLatin1String("cpp"))
        return CPPFILE;

    if(suffix == QLatin1String("S") || suffix == QLatin1String("s"))
        return ASFILE;

    if(suffix == QLatin1String("h") || suffix == QLatin1String("hpp"))
        return HEADERFILE;

    if(suffix == QLatin1String("ld") || suffix == QLatin1String("LD"))
        return LDFILE;

    return OTHER;
}

static bool kitMatcher(const ProjectExplorer::Kit* kit)
{
    Core::Id id = ProjectExplorer::DeviceTypeKitInformation::deviceTypeId(kit);
    bool result = id == BareMetal::Constants::BareMetalOsType;
    return result;
}

} // namespace Internal
} // namespace ArmProjectManage

#endif // UTILS_H
