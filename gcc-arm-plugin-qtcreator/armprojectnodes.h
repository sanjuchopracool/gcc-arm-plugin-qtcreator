#ifndef ARMPROJECTNODES_H
#define ARMPROJECTNODES_H
#include <projectexplorer/projectnodes.h>
namespace ArmPluginQtCreator {
namespace Internal {

class ArmProject;

class ArmProjectNode :public ProjectExplorer::ProjectNode
{
public:
    ArmProjectNode(ArmProject* project);
    ~ArmProjectNode();
    bool canAddSubProject(const QString &proFilePath) const;
    bool addSubProjects(const QStringList &proFilePaths);
    bool removeSubProjects(const QStringList &proFilePaths);

    QList<ProjectExplorer::ProjectAction> supportedActions(Node *node) const;

    bool addFiles(const QStringList &filePaths, QStringList *notAdded = 0);
    bool removeFiles(const QStringList &filePaths, QStringList *notRemoved = 0);
    bool deleteFiles(const QStringList &filePaths);
    bool renameFile(const QString &filePath, const QString &newFilePath);

    void createOrDeleteHeaderNode(bool create = true);
    void createOrDeleteSourceNode(bool create = true);
    void createOrDeleteOtherNode(bool create = true);

    void addOrRemoveHeaderFiles(const QStringList& headerFiles, bool add = true);
    void addOrRemoveSourceFiles(const QStringList& sourceFiles, bool add = true);
    void addOrRemoveOtherFiles(const QStringList& otherFiles, bool add = true);
    ProjectExplorer::FileType fileTypeFromExtension(const QString& filePath);

    QStringList includePathForAddedFiles() const;

private:
    void filterFilesBasedOnExtensions(const QStringList& filesToFilter, QStringList& headers,
                                      QStringList& sources, QStringList& others) const;

    void createFolderHash(QHash<QString, QStringList>& folderFilesHash, const QStringList& files) const;
    void addOrRemoveFilesForTheseDirectory(QMap<QString, ProjectExplorer::FolderNode*>& directoryNode,
                                           ProjectExplorer::FolderNode* parentNode,const QStringList &files,ProjectExplorer::FileType,
                                           bool add = true);

private:
    ArmProject* m_project;
    ProjectExplorer::FolderNode* m_headerNode;
    ProjectExplorer::FolderNode* m_sourcesNode;
    ProjectExplorer::FolderNode* m_otherNode;
    QString m_projectDirectoyString;
    QMap<QString, ProjectExplorer::FolderNode*> m_headerIncludeDirectoryNodes;
    QMap<QString, ProjectExplorer::FolderNode*> m_SourcesFilesDirectoryNodes;
    QMap<QString, ProjectExplorer::FolderNode*> m_OthersFilesDirectoryNodes;
};

class IncludePathNode : public ProjectExplorer::VirtualFolderNode
{
public:
    IncludePathNode(const QString& folderPath);
    ~IncludePathNode();
    QList<ProjectExplorer::ProjectAction> supportedActions(Node *node) const;
    void updateFiles();
};

class ArmProjectHeaderFile : public ProjectExplorer::FileNode
{
public:
    ArmProjectHeaderFile(const QString& path);
    QList<ProjectExplorer::ProjectAction> supportedActions(Node *node) const;
    ~ArmProjectHeaderFile();
};

class ArmProjectFileNode : public ProjectExplorer::FileNode
{
public:
    ArmProjectFileNode(const QString& path, ProjectExplorer::FileType type);
    QList<ProjectExplorer::ProjectAction> supportedActions(Node *node) const;
    ~ArmProjectFileNode();
};

class MyVirtualNode : public ProjectExplorer::VirtualFolderNode
{
public:
    MyVirtualNode(const QString& folderPath, int priority);
    ~MyVirtualNode();
    QList<ProjectExplorer::ProjectAction> supportedActions(Node *node) const;
};

} // namespace Internal
} // namespace ArmProjectManager

#endif // ARMPROJECTNODES_H
