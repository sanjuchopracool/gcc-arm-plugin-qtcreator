#include "armpluginqtcreatorplugin.h"
#include "armpluginqtcreatorconstants.h"
#include "armprojectmanager.h"
#include "armproject.h"
#include "armprojectbuildconfigurationfactory.h"
#include "armprojectgenericstep.h"
#include <coreplugin/icore.h>
#include <coreplugin/icontext.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/actionmanager/command.h>
#include <coreplugin/actionmanager/actioncontainer.h>
#include <coreplugin/coreconstants.h>
#include <utils/mimetypes/mimedatabase.h>
#include <projectexplorer/projectexplorerconstants.h>
#include <projectexplorer/projectexplorer.h>
#include "armprojectwizardfactory.h"
#include <projectexplorer/projecttree.h>

#include <QAction>
#include <QMessageBox>
#include <QMainWindow>
#include <QMenu>
#include <QFileDialog>

#include <QtPlugin>
using namespace Core;
using namespace ProjectExplorer;

namespace ArmPluginQtCreator {
namespace Internal {

ArmPluginQtCreatorPlugin::ArmPluginQtCreatorPlugin()
    : m_currentProject(0)
{
    // Create your members
}

ArmPluginQtCreatorPlugin::~ArmPluginQtCreatorPlugin()
{
    // Unregister objects from the plugin manager's object pool
    // Delete members
}

bool ArmPluginQtCreatorPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    // Register objects in the plugin manager's object pool
    // Load settings
    // Add actions to menus
    // Connect to other plugins' signals
    // In the initialize function, a plugin can be sure that the plugins it
    // depends on have initialized their members.

    Q_UNUSED(arguments)
    const QLatin1String mimetypesXml(":mimetype/ArmProject.mimetypes.xml");

    Utils::MimeDatabase::addMimeTypes(mimetypesXml);
    connect(ProjectTree::instance(), SIGNAL(aboutToShowContextMenu(ProjectExplorer::Project*,ProjectExplorer::Node*)),
            this, SLOT(updateContextActions(ProjectExplorer::Project*)));

    //find project context menus
    Core::ActionContainer *mproject =
            Core::ActionManager::actionContainer(ProjectExplorer::Constants::M_PROJECTCONTEXT);

    QAction *includePathActions = new QAction(tr("Manage include paths"), this);
    includePathActions->setToolTip(tr("All the header files in include paths will be added to the project"));
    Core::Command *cmd = Core::ActionManager::registerAction(includePathActions, Constants::ADD_INCLUDE_PATHS_ACTION_ID,
                                                             Core::Context(Constants::PROJECTCONTEXT));
//    cmd->setDefaultKeySequence(QKeySequence(tr("Ctrl+Alt+Meta+A")));
    cmd->setAttribute(Core::Command::CA_Hide);
    mproject->addAction(cmd, ProjectExplorer::Constants::G_PROJECT_BUILD);
    connect(includePathActions, SIGNAL(triggered()), this, SLOT(slotAddIncludePaths()));

    QAction *addExistingFileActios = new QAction(tr("Add Exisiting Files"), this);
    addExistingFileActios->setToolTip(tr("Add exisiting files in project, files will not be copied to project folder"));
    cmd = Core::ActionManager::registerAction(addExistingFileActios, Constants::ADD_EXISTING_FILES_ACTION_ID,
                                                             Core::Context(Constants::PROJECTCONTEXT));
//    cmd->setDefaultKeySequence(QKeySequence(tr("Ctrl+Alt+Meta+A")));
    cmd->setAttribute(Core::Command::CA_Hide);
    mproject->addAction(cmd, ProjectExplorer::Constants::G_PROJECT_FILES);
    connect(addExistingFileActios, SIGNAL(triggered()), this, SLOT(slotAddExistingFiles()));

    QAction *projectPropertiesAction = new QAction(tr("Properties"), this);
    projectPropertiesAction->setToolTip(tr("Change project properties"));
    Core::Command *propertyCmd = Core::ActionManager::registerAction(projectPropertiesAction, Constants::PROJECT_PROPERTIES_ACTION_ID,
                                                             Core::Context(Constants::PROJECTCONTEXT));
//    cmd->setDefaultKeySequence(QKeySequence(tr("Ctrl+Alt+Meta+A")));
    propertyCmd->setAttribute(Core::Command::CA_Hide);
    mproject->addAction(propertyCmd, ProjectExplorer::Constants::G_PROJECT_LAST);
    connect(projectPropertiesAction, SIGNAL(triggered()), this, SLOT(slotShowProjectPropertyDialog()));


    addAutoReleasedObject(new ArmProjectManager()); //register project manager
    addAutoReleasedObject(new ArmProjectBuildConfigurationFactory()); // build configuration
    addAutoReleasedObject(new ArmProjectBuildStepFactory());
//    addAutoReleasedObject(new ArmProjectWizardFactory());
    return true;
}

void ArmPluginQtCreatorPlugin::extensionsInitialized()
{
    // Retrieve objects from the plugin manager's object pool
    // In the extensionsInitialized function, a plugin can be sure that all
    // plugins that depend on it are completely initialized.
}

ExtensionSystem::IPlugin::ShutdownFlag ArmPluginQtCreatorPlugin::aboutToShutdown()
{
    // Save settings
    // Disconnect from signals that are not needed during shutdown
    // Hide UI (if you add UI that is not in the main window directly)
    return SynchronousShutdown;
}

void ArmPluginQtCreatorPlugin::updateContextActions(ProjectExplorer::Project *project)
{
    m_currentProject = project;
}

void ArmPluginQtCreatorPlugin::slotAddIncludePaths()
{
    ArmProject* armProject = qobject_cast<ArmProject*>(m_currentProject);
    if(armProject)
        armProject->manageIncludePaths();
}

void ArmPluginQtCreatorPlugin::slotAddExistingFiles()
{
    ArmProject* armProject = qobject_cast<ArmProject*>(m_currentProject);
    if(armProject)
    {
        QStringList fileNames = QFileDialog::getOpenFileNames(ICore::mainWindow(),
            tr("Add Existing Files"), armProject->projectDirectory().toString());
        if (fileNames.isEmpty())
            return;
        armProject->addFiles(fileNames);
    }
}

void ArmPluginQtCreatorPlugin::slotShowProjectPropertyDialog()
{
    ArmProject* armProject = qobject_cast<ArmProject*>(m_currentProject);
    if(armProject)
        armProject->showSettingDialog();
}

} // namespace Internal
} // namespace ArmPluginQtCreator
