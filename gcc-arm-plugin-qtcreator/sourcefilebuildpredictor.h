#ifndef SOURCEFILEBUILDPREDICTOR_H
#define SOURCEFILEBUILDPREDICTOR_H

#include <QStringList>
#include <projectexplorer/processparameters.h>
#include <QProcess>
#include <utils/qtcprocess.h>
#include <QDateTime>

namespace ArmPluginQtCreator {
namespace Internal {

class SourceFileBuildPredictor
{
public:
    SourceFileBuildPredictor(const QString& compiler);
    ~SourceFileBuildPredictor();

    bool needCompilation(const QString& sourceFilePath, const QDateTime& objFileLastModified) const;

    //first set include pathsand then defines
    void setIncludePaths(const QString &includePaths);
    void setDefines(const QString &m_defines);
    void setProcessParameters(const ProjectExplorer::ProcessParameters& param);

private:
    QString m_arguments;
    QString m_compilerCommand;
    ProjectExplorer::ProcessParameters m_param;
    Utils::QtcProcess* m_process;
};

} // namespace Internal
} // namespace ArmProjectManager

#endif // SOURCEFILEBUILDPREDICTOR_H
