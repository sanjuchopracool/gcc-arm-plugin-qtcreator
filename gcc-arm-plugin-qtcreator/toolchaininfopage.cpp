#include "toolchaininfopage.h"
#include "ui_toolchaininfopage.h"
#include "armproject.h"

namespace ArmPluginQtCreator {
namespace Internal {

ToolChainInfoPage::ToolChainInfoPage(ArmProjectBuildSetting& buildSetting, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ToolChainInfoPage)
{
    ui->setupUi(this);
    const ArmToolChain& toolChain = buildSetting.toolChain();
    ui->lineEditTooChain->setText(toolChain.directory.absolutePath());
    ui->lineEditPrefix->setText(toolChain.prefix);
    QStringList tools;

    tools <<toolChain.cppCompiler() <<
            toolChain.cCompiler() <<
            toolChain.asembler() <<
            toolChain.linker() <<
            toolChain.objcopy() <<
            toolChain.objdump() <<
            toolChain.size();

    ui->textEdit->setPlainText(tools.join(QLatin1Char('\n')));
}

ToolChainInfoPage::~ToolChainInfoPage()
{
    delete ui;
}

//////////////////////////////////////////////////////////////////////////////////////////////
/// \brief ToolChainInfoWidget::ToolChainInfoWidget
/// \param project
/// \param parent
//////////////////////////////////////////////////////////////////////////////////////////////

ToolChainInfoWidget::ToolChainInfoWidget(ArmProjectBuildSetting& buildSetting, QWidget *parent)
    : ToolChainInfoPage(buildSetting, parent),
      m_icon(QIcon(QLatin1String(":/images/toolchain.png")))
{

}

ToolChainInfoWidget::~ToolChainInfoWidget()
{

}

QString ToolChainInfoWidget::name() const
{
    return tr("Tool Chain");
}

const QIcon &ToolChainInfoWidget::icon() const
{
    return m_icon;
}

QWidget *ToolChainInfoWidget::widget()
{
    return this;
}

void ToolChainInfoWidget::apply()
{
    //do nothing its a read only page
}


} // namespace Internal
} // namespace ArmProjectManager
