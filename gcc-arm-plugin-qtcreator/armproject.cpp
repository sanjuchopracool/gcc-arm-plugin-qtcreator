#include "armproject.h"
#include "armpluginqtcreatorconstants.h"
#include "armprojectmanager.h"
#include "armprojectnodes.h"
#include "includepathconfigurationwidget.h"
#include "Utils.h"
#include "armprojectnodes.h"
#include "armprojectsettingdialog.h"
#include <baremetal/baremetalconstants.h>
#include <projectexplorer/projectexplorerconstants.h>
#include <projectexplorer/kitinformation.h>
#include <coreplugin/id.h>
#include <coreplugin/icontext.h>
#include <coreplugin/icore.h>
#include <projectexplorer/kitmanager.h>
#include <projectexplorer/kit.h>
#include <projectexplorer/target.h>
#include <cpptools/cppmodelmanager.h>
#include <cpptools/projectpartbuilder.h>
#include <projectexplorer/buildtargetinfo.h>
#include <QDomDocument>
#include <QDomElement>
#include <QMessageBox>
#include <QDialogButtonBox>
#include <QDialog>
#include <QVBoxLayout>

namespace ArmPluginQtCreator {
namespace Internal {

ArmProject::ArmProject(ArmProjectManager *manager, const QString &filename)
    : m_projectManager(manager),
      m_fileName(filename),
      m_rootNode(0),
      m_projectBuildSetting(this)
{
    setId(Constants::ARM_PROJECT_ID);
    setProjectContext(Core::Context(Constants::PROJECTCONTEXT));
    setProjectLanguages(Core::Context(ProjectExplorer::Constants::LANG_CXX));
    QFileInfo fileInfo(m_fileName);
    m_projectName = fileInfo.baseName();
    m_projectDir = fileInfo.absoluteDir();
    setDocument( new ArmProjectFileDocument(this, m_fileName) );
    m_projectManager->registerProject(this);
    //create structure in the end
    m_rootNode = new ArmProjectNode(this);
    loadProject();
    connectPostLoadingConnections();//After loading connect all signals

}

ArmProject::~ArmProject()
{
    m_codeModelFuture.cancel();
    delete m_rootNode;
}

QString ArmProject::displayName() const
{
    return m_projectName;
}

ProjectExplorer::IProjectManager *ArmProject::projectManager() const
{
    return m_projectManager;
}

ProjectExplorer::ProjectNode *ArmProject::rootProjectNode() const
{
    return m_rootNode;
}

QStringList ArmProject::files(ProjectExplorer::Project::FilesMode fileMode) const
{
    Q_UNUSED(fileMode)
    QStringList headerFiles = m_headerFiles + m_sourceFiles + m_otherFiles;
    Q_FOREACH(QString folder, m_userIncludePathNodeMap.keys())
    {
        Q_FOREACH(ProjectExplorer::FileNode* node,m_userIncludePathNodeMap.value(folder)->fileNodes())
            headerFiles.append(node->filePath().toString());
    }
    return headerFiles;
}

void ArmProject::slotUserIncludePathsModified()
{
    QStringList existingList = m_userIncludePathNodeMap.keys();
    QStringList foldersToRemoveList;
    QStringList foldersToAddList;
    filterLists(existingList, m_projectBuildSetting.userIncludePaths(), foldersToAddList, foldersToRemoveList);

    //make sure project path is not here because it would be always there as Include Node
    foldersToAddList.removeOne(this->projectDirectory().toString());

    if(!foldersToAddList.count() &&  !foldersToRemoveList.count())
        return;

    Q_FOREACH(QString folderToAdd, foldersToAddList)
    {
        IncludePathNode* includePathNode = new IncludePathNode(folderToAdd);
        m_rootNode->addFolderNodes(QList<ProjectExplorer::FolderNode*>() << includePathNode);
        includePathNode->setDisplayName(relativeFilePath(folderToAdd));
        includePathNode->updateFiles();
        m_userIncludePathNodeMap.insert(folderToAdd, includePathNode);
    }

    Q_FOREACH(QString folderToRemove, foldersToRemoveList)
    {
        IncludePathNode* folderToRemoveNode = m_userIncludePathNodeMap.value(folderToRemove);
        if(folderToRemoveNode)
        {
            //it should ideally delete all the child nodes; its deleting i checked ;)
            m_rootNode->removeFolderNodes(QList<ProjectExplorer::FolderNode*>() << folderToRemoveNode);
            m_userIncludePathNodeMap.remove(folderToRemove);
        }
    }
    emit modified();
}

void ArmProject::addOrRemoveHeaderFiles(const QStringList &headerFiles, bool add)
{
    if(headerFiles.isEmpty())
        return;

    QStringList filesToAlter = filterExisitingFiles(headerFiles, m_headerFiles, add);
    if(!filesToAlter.count())
        return;

    m_rootNode->addOrRemoveHeaderFiles(filesToAlter, add);
    addOrRemoveListInExisitingList(m_headerFiles, filesToAlter, add);
    emit modified();
}

void ArmProject::addOrRemoveSourceFiles(const QStringList &sourceFiles, bool add)
{
    if(sourceFiles.isEmpty())
        return;

    QStringList filesToAlter = filterExisitingFiles(sourceFiles, m_sourceFiles, add);
    if(!filesToAlter.count())
        return;

    m_rootNode->addOrRemoveSourceFiles(filesToAlter, add);
    addOrRemoveListInExisitingList(m_sourceFiles, filesToAlter, add);
    emit modified();
}

void ArmProject::addOrRemoveOtherFiles(const QStringList &otherFiles, bool add)
{
    if(otherFiles.isEmpty())
        return;

    QStringList filesToAlter = filterExisitingFiles(otherFiles, m_otherFiles, add);
    if(!filesToAlter.count())
        return;

    m_rootNode->addOrRemoveOtherFiles(filesToAlter, add);
    addOrRemoveListInExisitingList(m_otherFiles, filesToAlter, add);
    // handle the linker files setting
    const LinkerSetting& linkerSetting = m_projectBuildSetting.linkerSetting();
    LinkerSetting newLinkerSetting = linkerSetting;
    if(add)
    {
        if(linkerSetting.linkerFile.isEmpty())
        {
            QStringList ldFiles = linkerFiles();
            if(ldFiles.count())
                newLinkerSetting.linkerFile = ldFiles.first();
        }
    }
    else
    {
        QStringList ldFiles = linkerFiles();
        if(!ldFiles.contains(linkerSetting.linkerFile))
        {
            if(ldFiles.count())
                newLinkerSetting.linkerFile = ldFiles.first();
            else
                newLinkerSetting.linkerFile.clear();
        }
    }
    m_projectBuildSetting.setLinkerSetting(newLinkerSetting);
    emit modified();
}

bool ArmProject::renameFile(const QString &filePath, const QString &newFilePath)
{
    ProjectExplorer::FileType oldFiletype = m_rootNode->fileTypeFromExtension(filePath);
    ProjectExplorer::FileType newFiletype = m_rootNode->fileTypeFromExtension(newFilePath);
    QStringList filesToRemove;
    QStringList filesToAdd;
    filesToRemove << filePath;
    filesToAdd << newFilePath;

    switch (oldFiletype)
    {
    case ProjectExplorer::FileType::Header:
        addOrRemoveHeaderFiles(filesToRemove, false);
        break;
    case ProjectExplorer::FileType::Source:
        addOrRemoveSourceFiles(filesToRemove, false);
        break;
    case ProjectExplorer::FileType::Unknown:
        addOrRemoveOtherFiles(filesToRemove, false);
        break;
    default:
        break;
    }

    switch (newFiletype)
    {
    case ProjectExplorer::FileType::Header:
        addOrRemoveHeaderFiles(filesToAdd, true);
        break;
    case ProjectExplorer::FileType::Source:
        addOrRemoveSourceFiles(filesToAdd, true);
        break;
    case ProjectExplorer::FileType::Unknown:
        addOrRemoveOtherFiles(filesToAdd, true);
        break;
    default:
        break;
    }

    emit modified();
    return true;
}

bool ArmProject::supportsKit(ProjectExplorer::Kit *k, QString *errorMessage) const
{
    Core::Id id = ProjectExplorer::DeviceTypeKitInformation::deviceTypeId(k);
    if(id != BareMetal::Constants::BareMetalOsType)
    {
        if(errorMessage)
            *errorMessage = tr("Arm projects are only supported br BareMetal devices");

        return false;
    }

    return true;
}

QStringList ArmProject::allIncludePaths() const
{
    return m_projectBuildSetting.userIncludePaths() + m_rootNode->includePathForAddedFiles();
}

QStringList ArmProject::sourceFiles() const
{
    return m_sourceFiles;
}

QStringList ArmProject::linkerFiles() const
{
    QStringList linkerFiles;
    Q_FOREACH(const QString& filePath, m_otherFiles)
    {
        if(extensionType(filePath) == LDFILE)
            linkerFiles.append(filePath);
    }

    return linkerFiles;
}

void ArmProject::updateApplicationTargets()
{
    ProjectExplorer::Target* target = activeTarget();
    if(!activeTarget())
        return;

    //this thing also need to be changed
    ProjectExplorer::BuildTargetInfoList appTargetList;
    Utils::FileName executableFile = Utils::FileName::fromString(projectDirectory().toString() + QLatin1Char('/') + m_projectName + QLatin1String(".elf"));
    appTargetList.list << ProjectExplorer::BuildTargetInfo(m_projectName,
                                                           executableFile,
                                                           Utils::FileName::fromString(m_fileName));
    target->setApplicationTargets(appTargetList);
    target->updateDefaultRunConfigurations();
}

QString ArmProject::relativeFilePath(const QString &absolutePath) const
{
    return m_projectDir.relativeFilePath(absolutePath);
}

QString ArmProject::absoluteFilePath(const QString &relativePath) const
{
    return QDir::cleanPath(m_projectDir.absoluteFilePath(relativePath));
}

QStringList ArmProject::relativeFilePaths(const QStringList &absoluteFilePaths) const
{
    QStringList relativePaths;
    Q_FOREACH(const QString& filePath, absoluteFilePaths)
        relativePaths.append(relativeFilePath(filePath));

    return relativePaths;
}

QStringList ArmProject::absoluteFilePaths(const QStringList &relativeFilePaths) const
{
    QStringList absolutePaths;
    Q_FOREACH(const QString& filePath, relativeFilePaths)
        absolutePaths.append(absoluteFilePath(filePath));

    return absolutePaths;
}

ArmProjectBuildSetting &ArmProject::projectBuildSetting()
{
    return m_projectBuildSetting;
}

ProjectExplorer::Project::RestoreResult ArmProject::fromMap(const QVariantMap &map, QString *errorMessage)
{
    using namespace ProjectExplorer;
    RestoreResult result = Project::fromMap(map, NULL);
    if(result != RestoreResult::Ok)
        return result;

    //for new project there might not be any kit
    if(!activeTarget())
    {
//        QDialog dialog(Core::ICore::mainWindow());
//        ProjectExplorer::TargetSetupPage targetPage(&dialog);
//        targetPage.setTitle(QLatin1String("Choose Arm toolkit(Bare Metal Device"));
//        targetPage.setRequiredKitMatcher(ProjectExplorer::KitMatcher(kitMatcher));
//        targetPage.initializePage();
//        QVBoxLayout layout(&dialog);
//        QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal, &dialog);
//        connect(&buttonBox, SIGNAL(accepted()), &dialog, SLOT(accept()));
//        connect(&buttonBox, SIGNAL(rejected()), &dialog, SLOT(reject()));
//        layout.addWidget(&targetPage);
//        layout.addWidget(&buttonBox);
//        dialog.setLayout(&layout);
//        if(dialog.exec() == QDialog::Accepted)
//            qDebug() << "Selected Kits count: " << targetPage.selectedKits().count();

        //first check the kit to be add in project
        QList<ProjectExplorer::Kit*> supportedKits;
        QList<ProjectExplorer::Kit*> kits = ProjectExplorer::KitManager::kits();
        Q_FOREACH(ProjectExplorer::Kit* kit, kits)
        {
            if(this->supportsKit(kit))
                supportedKits.append(kit);
        }

        if(supportedKits.isEmpty())
        {
            //show message
            return RestoreResult::Error;
        }

        //create target for first kits
        ProjectExplorer::Target* target = createTarget(supportedKits.first());
        addTarget(target);
        updateApplicationTargets();
    }

    updateCodeEditorTools();
    return RestoreResult::Ok;
}

QStringList ArmProject::filterExisitingFiles(const QStringList &newFiles, const QStringList &exisitngFiles, bool add)
{
    QStringList filesToAlter;
    Q_FOREACH(QString filePath, newFiles)
    {
        if(add && !exisitngFiles.contains(filePath))
            filesToAlter.append(filePath);
        else if (!add && exisitngFiles.contains(filePath))
            filesToAlter.append(filePath);
    }

    return filesToAlter;
}

void ArmProject::addOrRemoveListInExisitingList(QStringList &existingList, const QStringList &changes, bool add)
{
    if(add)
        existingList.append(changes);
    else
    {
        Q_FOREACH(QString fileName, changes)
            existingList.removeOne(fileName);
    }
}

void ArmProject::loadProject()
{
    ///NOTE Saving and loading use relativ paths
    QFileInfo projectFileInfo(m_fileName);
    if(!projectFileInfo.exists())
        return;

    QFile projectFile(m_fileName);
    if(projectFile.open(QIODevice::ReadOnly))
    {
        QDomDocument doc;
        if(doc.setContent(projectFile.readAll()))
        {
            QDomElement rootE = doc.documentElement();
            if(!rootE.isNull())
            {
                QDomElement includedFilesE = rootE.firstChildElement(QLatin1String("Files"));
                if(!includedFilesE.isNull())
                {
                    QStringList files;
                    QDomElement fileE = includedFilesE.firstChildElement(QLatin1String("file"));
                    while(!fileE.isNull())
                    {
                        files.append(fileE.text());
                        fileE = fileE.nextSiblingElement();
                    }
                    m_rootNode->addFiles(absoluteFilePaths(files));
                }

                m_projectBuildSetting.load(rootE, doc);
            }
        }
    }
    slotUserIncludePathsModified(); //setting is loaded not that singal will not come
}

void ArmProject::connectPostLoadingConnections()
{
    connect(&m_projectBuildSetting, SIGNAL(includePathsModified()),
            this, SLOT(slotUserIncludePathsModified()));
    connect(this, SIGNAL(modified()), this, SLOT(saveProject()));
    connect(this, SIGNAL(modified()), this, SLOT(updateCodeEditorTools()));
    connect(&m_projectBuildSetting, SIGNAL(settingModified()), this, SLOT(saveProject()));
    connect(this, SIGNAL(activeTargetChanged(ProjectExplorer::Target*)),
            &m_projectBuildSetting, SLOT(updateToolChain()));
}

void ArmProject::filterLists(const QStringList &exisitingList, const QStringList &toAdd,
                             QStringList &added, QStringList &removed)
{
    Q_FOREACH(QString path, toAdd)
    {
        if(!exisitingList.contains(path))
            added.append(path);
    }

    Q_FOREACH(QString path, exisitingList)
    {
        if(!toAdd.contains(path))
            removed.append(path);
    }
}

void ArmProject::updateCodeEditorTools()
{
    ProjectExplorer::Target* target = activeTarget();
    if(target)
    {
        ProjectExplorer::Kit* kit = target->kit();
        if(kit)
        {
            CppTools::CppModelManager *modelManager =
                    CppTools::CppModelManager::instance();

            if (modelManager)
            {
                m_codeModelFuture.cancel();
                CppTools::ProjectInfo pInfo = CppTools::ProjectInfo(this);
                CppTools::ProjectPartBuilder ppBuilder(pInfo);
                const QStringList& symbols = m_projectBuildSetting.symbols();
                if(symbols.count())
                {
                    QString projectDefine = QLatin1String("#define ") + symbols.join(QLatin1String("\n#define "));
                    ppBuilder.setDefines(projectDefine.toLatin1());
                }
;
                QStringList allProjectPaths = m_projectBuildSetting.userIncludePaths();
                allProjectPaths << m_rootNode->includePathForAddedFiles();
                allProjectPaths << projectDirectory().toString();
                ppBuilder.setIncludePaths(allProjectPaths);
                const QList<Core::Id> languages = ppBuilder.createProjectPartsForFiles(files(FilesMode::AllFiles));
                foreach (Core::Id language, languages)
                    setProjectLanguage(language, true);

                pInfo.finish();
                m_codeModelFuture = modelManager->updateProjectInfo(pInfo);
            }
        }
    }
}

void ArmProject::saveProject()
{
    ///NOTE Saving and loading use relativ paths
    QFileInfo projectFileInfo(m_fileName);
    if(!projectFileInfo.exists())
        return;

    QFile projectFile(m_fileName);
    if(projectFile.open(QIODevice::WriteOnly | QIODevice::Truncate))
    {
        QDomDocument doc;
        QDomElement rootE = doc.createElement(QLatin1String("ChopsProject"));
        doc.appendChild(rootE);

        QStringList relativePaths;
        QDomElement includedFilesE = doc.createElement(QLatin1String("Files"));
        relativePaths = relativeFilePaths(m_headerFiles + m_sourceFiles + m_otherFiles);
        Q_FOREACH(const QString& path, relativePaths)
        {
            QDomElement fileE = doc.createElement(QLatin1String("file"));
            fileE.appendChild(doc.createTextNode(path));
            includedFilesE.appendChild(fileE);
        }
        rootE.appendChild(includedFilesE);

        m_projectBuildSetting.save(rootE, doc);
        projectFile.write(doc.toByteArray());
    }
}

void ArmProject::manageIncludePaths()
{
    IncludePathConfigurationDialog dialog(Core::ICore::mainWindow());
    dialog.setIncludePaths(m_projectBuildSetting.userIncludePaths());
    if(dialog.exec() == QDialog::Accepted)
        m_projectBuildSetting.setUserIncludePaths(dialog.includePaths());
}

void ArmProject::addFiles(const QStringList &files)
{
    m_rootNode->addFiles(files);
}

void ArmProject::showSettingDialog()
{
    ArmProjectSettingDialog dialog(m_projectBuildSetting, Core::ICore::mainWindow());
    dialog.exec();
}

ArmProjectFileDocument::ArmProjectFileDocument(ArmProject *parent, QString fileName)
    : Core::IDocument(parent)
{
    setId(Constants::PROJECT_FILE_ID);
    setMimeType(QLatin1String(Constants::ARM_PROJECT_MIME_TYPE));
    setFilePath(Utils::FileName::fromString(fileName));
}

ArmProjectFileDocument::~ArmProjectFileDocument()
{

}

QString ArmProjectFileDocument::defaultPath() const
{
    return QString();
}

QString ArmProjectFileDocument::suggestedFileName() const
{
    return QString();
}

bool ArmProjectFileDocument::isModified() const
{
    return false;
}

bool ArmProjectFileDocument::isSaveAsAllowed() const
{
    return false;
}

bool ArmProjectFileDocument::save(QString *errorString, const QString &fileName, bool autoSave)
{
    Q_UNUSED(errorString)
    Q_UNUSED(fileName)
    Q_UNUSED(autoSave)
    return false;
}

bool ArmProjectFileDocument::reload(QString *errorString, Core::IDocument::ReloadFlag flag, Core::IDocument::ChangeType type)
{
    Q_UNUSED(errorString)
    Q_UNUSED(flag)
    Q_UNUSED(type)
    return false;
}


} // namespace Internal
} // namespace ArmProjectManager
