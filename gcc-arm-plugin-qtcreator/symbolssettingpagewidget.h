#ifndef SYMBOLSSETTINGPAGEWIDGET_H
#define SYMBOLSSETTINGPAGEWIDGET_H

#include "includepathconfigurationwidget.h"
#include "ISettingPageWidget.h"
#include "armprojectbuildsetting.h"

namespace ArmPluginQtCreator {
namespace Internal {

class ArmProject;
class SymbolsSettingPageWidget: public StringListManagementWidget,
        virtual public ISettingPageWidget
{
public:
    SymbolsSettingPageWidget(ArmProjectBuildSetting& buildSetting, QWidget* parent = 0);
    ~SymbolsSettingPageWidget();

    QString name() const;
    const QIcon& icon() const;
    QWidget* widget();
    void apply();

private:
    ArmProjectBuildSetting& m_buildSetting;
    QIcon m_icon;
};

} // namespace Internal
} // namespace ArmProjectManager

#endif // SYMBOLSSETTINGPAGEWIDGET_H
