#include "armprojectbuildconfigurationfactory.h"
#include "armpluginqtcreatorconstants.h"
#include "armproject.h"

#include <projectexplorer/target.h>
#include <projectexplorer/buildinfo.h>
#include <utils/mimetypes/mimedatabase.h>
#include <projectexplorer/buildconfiguration.h>
#include <projectexplorer/projectexplorerconstants.h>
#include <projectexplorer/buildsteplist.h>
#include "armprojectgenericstep.h"
#include <utils/qtcassert.h>
#include <QFormLayout>

using namespace ProjectExplorer;
namespace ArmPluginQtCreator {
namespace Internal {

ArmProjectBuildConfiguration::ArmProjectBuildConfiguration(Target *parent)
    : BuildConfiguration(parent, Core::Id(Constants::ARM_PROJECT_BC_ID))
{

}

ArmProjectBuildConfiguration::~ArmProjectBuildConfiguration()
{

}

NamedWidget *ArmProjectBuildConfiguration::createConfigWidget()
{
    return new ArmProjectBuildSettingsWidget(this);
}

BuildConfiguration::BuildType ArmProjectBuildConfiguration::buildType() const
{
    return BuildConfiguration::Unknown;
}

ArmProjectBuildConfiguration::ArmProjectBuildConfiguration(Target *parent, ArmProjectBuildConfiguration *source)
    : BuildConfiguration(parent, source)
{
    cloneSteps(source);
}

ArmProjectBuildConfiguration::ArmProjectBuildConfiguration(Target *parent, Core::Id id)
    : BuildConfiguration(parent, id)
{

}

ArmProjectBuildConfigurationFactory::ArmProjectBuildConfigurationFactory(QObject *parent) :
    IBuildConfigurationFactory(parent)
{
}

ArmProjectBuildConfigurationFactory::~ArmProjectBuildConfigurationFactory()
{

}

int ArmProjectBuildConfigurationFactory::priority(const Target *t) const
{
    return canHandle(t) ? 0 : -1;
}

int ArmProjectBuildConfigurationFactory::priority(const Kit *k, const QString &projectPath) const
{
    Utils::MimeDatabase mdb;
    if (k && mdb.mimeTypeForFile(projectPath).matchesName(QLatin1String(Constants::ARM_PROJECT_MIME_TYPE)))
        return 0;
    return -1;
}

QList<BuildInfo *> ArmProjectBuildConfigurationFactory::availableBuilds(const Target *parent) const
{
    QList<ProjectExplorer::BuildInfo *> result;
    BuildInfo *info = createBuildInfo(parent->kit(), parent->project()->projectDirectory());
    result << info;
    return result;
}

QList<BuildInfo *> ArmProjectBuildConfigurationFactory::availableSetups(const Kit *k, const QString &projectPath) const
{
    QList<BuildInfo *> result;
    BuildInfo *info = createBuildInfo(k, ProjectExplorer::Project::projectDirectory(Utils::FileName::fromString(projectPath)));
    //: The name of the build configuration created by default for a generic project.
    info->displayName = tr("Default");
    result << info;
    return result;
}

BuildConfiguration *ArmProjectBuildConfigurationFactory::create(Target *parent, const BuildInfo *info) const
{
    QTC_ASSERT(info->factory() == this, return 0);
    QTC_ASSERT(info->kitId == parent->kit()->id(), return 0);
    QTC_ASSERT(!info->displayName.isEmpty(), return 0);

    ArmProjectBuildConfiguration *bc = new ArmProjectBuildConfiguration(parent);
    bc->setDisplayName(info->displayName);
    bc->setDefaultDisplayName(info->displayName);
    bc->setBuildDirectory(info->buildDirectory);

    BuildStepList *buildSteps = bc->stepList(ProjectExplorer::Constants::BUILDSTEPS_BUILD);
    BuildStepList *cleanSteps = bc->stepList(ProjectExplorer::Constants::BUILDSTEPS_CLEAN);

    Q_ASSERT(buildSteps);
    ArmProjectGenericBuildStep *makeStep = new ArmProjectGenericBuildStep(buildSteps);
    buildSteps->insertStep(0, makeStep);

    Q_ASSERT(cleanSteps);
    ArmProjectGenericBuildStep *cleanStep = new ArmProjectGenericBuildStep(cleanSteps);
    cleanStep->setClean(true);
    cleanSteps->insertStep(0, cleanStep);

    return bc;
}

bool ArmProjectBuildConfigurationFactory::canRestore(const Target *parent, const QVariantMap &map) const
{
    if (!canHandle(parent))
        return false;
    return ProjectExplorer::idFromMap(map) == Constants::ARM_PROJECT_BC_ID;
}

BuildConfiguration *ArmProjectBuildConfigurationFactory::restore(Target *parent, const QVariantMap &map)
{
    if (!canRestore(parent, map))
        return 0;

    ArmProjectBuildConfiguration *bc(new ArmProjectBuildConfiguration(parent));
    if (bc->fromMap(map))
        return bc;

    delete bc;
    return 0;
}

bool ArmProjectBuildConfigurationFactory::canClone(const Target *parent, BuildConfiguration *source) const
{
    if (!canHandle(parent))
        return false;
    return source->id() == Constants::ARM_PROJECT_BC_ID;
}

BuildConfiguration *ArmProjectBuildConfigurationFactory::clone(Target *parent, BuildConfiguration *source)
{
    if (!canClone(parent, source))
        return 0;
    return new ArmProjectBuildConfiguration(parent, qobject_cast<ArmProjectBuildConfiguration *>(source));
}

bool ArmProjectBuildConfigurationFactory::canHandle(const Target *t) const
{
    if (!t->project()->supportsKit(t->kit()))
        return false;
    return qobject_cast<ArmProject *>(t->project());
}

BuildInfo *ArmProjectBuildConfigurationFactory::createBuildInfo(const Kit *k, const Utils::FileName &buildDir) const
{
    BuildInfo *info = new BuildInfo(this);
    info->typeName = tr("Build");
    info->buildDirectory = buildDir;
    info->kitId = k->id();
    return info;
}

////////////////////////////////////////////////////////////////////////////////////
// GenericBuildSettingsWidget
////////////////////////////////////////////////////////////////////////////////////

ArmProjectBuildSettingsWidget::ArmProjectBuildSettingsWidget(ArmProjectBuildConfiguration *bc)
    : m_buildConfiguration(0)
{
    QFormLayout *fl = new QFormLayout(this);
    fl->setContentsMargins(0, -1, 0, -1);
    fl->setFieldGrowthPolicy(QFormLayout::ExpandingFieldsGrow);

    // build directory
    m_pathChooser = new Utils::PathChooser(this);
    m_pathChooser->setHistoryCompleter(QLatin1String("Generic.BuildDir.History"));
    m_pathChooser->setEnabled(true);
    fl->addRow(tr("Build directory:"), m_pathChooser);
    connect(m_pathChooser, SIGNAL(changed(QString)), this, SLOT(buildDirectoryChanged()));

    m_buildConfiguration = bc;
    m_pathChooser->setBaseFileName(bc->target()->project()->projectDirectory());
    m_pathChooser->setEnvironment(bc->environment());
    m_pathChooser->setPath(m_buildConfiguration->rawBuildDirectory().toString());
    setDisplayName(tr("Generic Manager"));

    connect(bc, SIGNAL(environmentChanged()), this, SLOT(environmentHasChanged()));
}

void ArmProjectBuildSettingsWidget::buildDirectoryChanged()
{
    m_buildConfiguration->setBuildDirectory(Utils::FileName::fromString(m_pathChooser->rawPath()));
}

void ArmProjectBuildSettingsWidget::environmentHasChanged()
{
    m_pathChooser->setEnvironment(m_buildConfiguration->environment());
}

} // namespace Internal
} // namespace ArmProjectManager
