#ifndef TOOLCHAININFOPAGE_H
#define TOOLCHAININFOPAGE_H

#include <QWidget>
#include "ISettingPageWidget.h"
#include "armprojectbuildsetting.h"

namespace Ui {
class ToolChainInfoPage;
}

namespace ArmPluginQtCreator {
namespace Internal {

class ArmProject;

class ToolChainInfoPage : public QWidget
{
    Q_OBJECT

public:
    explicit ToolChainInfoPage(ArmProjectBuildSetting& buildSetting, QWidget *parent = 0);
    ~ToolChainInfoPage();

private:
    Ui::ToolChainInfoPage *ui;
};


////////////////////////////////////////////////////////////////////////////
/// \brief The ToolChainInfoWidget class
///
/// ////////////////////////////////////////////////////////////////////////

class ToolChainInfoWidget : public ToolChainInfoPage,
        public ISettingPageWidget
{
    Q_OBJECT
public:
    explicit ToolChainInfoWidget(ArmProjectBuildSetting& buildSetting, QWidget *parent = 0);
    ~ToolChainInfoWidget();

    QString name() const;
    const QIcon& icon() const;
    QWidget* widget();
    void apply();

private:
    QIcon m_icon;
};


} // namespace Internal
} // namespace ArmProjectManager

#endif // TOOLCHAININFOPAGE_H
