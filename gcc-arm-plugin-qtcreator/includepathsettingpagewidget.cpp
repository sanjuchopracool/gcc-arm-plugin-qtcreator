#include "includepathsettingpagewidget.h"
#include "armproject.h"

namespace ArmPluginQtCreator {
namespace Internal {


IncludePathSettingPageWidget::IncludePathSettingPageWidget(ArmProjectBuildSetting &buildSetting, QWidget *parent)
    : StringListManagementWidget(parent),
      m_buildSetting(buildSetting),
      m_icon(QIcon(QLatin1String(":/images/includepath.png")))
{
    this->setStringList(m_buildSetting.userIncludePaths());
}

IncludePathSettingPageWidget::~IncludePathSettingPageWidget()
{

}

QString IncludePathSettingPageWidget::name() const
{
    return tr("Include paths");
}

const QIcon &IncludePathSettingPageWidget::icon() const
{
    return m_icon;
}

QWidget *IncludePathSettingPageWidget::widget()
{
    return this;
}

void IncludePathSettingPageWidget::apply()
{
    m_buildSetting.setUserIncludePaths(this->stringList());
}


} // namespace Internal
} // namespace ArmProjectManager
