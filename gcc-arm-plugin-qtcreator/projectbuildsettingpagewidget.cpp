#include "projectbuildsettingpagewidget.h"
#include "ui_projectbuildsettingpage.h"
#include "comboselectionutilitywidget.h"
#include "armproject.h"
#include <QDebug>
#include <QFileDialog>

namespace ArmPluginQtCreator {
namespace Internal {

typedef QPair<QString, QString> Pair;
ProjectBuildSettingPageWidget::ProjectBuildSettingPageWidget(ArmProjectBuildSetting &buildSetting, QWidget *parent) :
    QWidget(parent),
    ISettingPageWidget(tr("Build Setting"), QLatin1String(":/images/build.png")),
    ui(new Ui::ProjectBuildSettingPage),
    m_projectBuildSetting(buildSetting)
{
    ui->setupUi(this);

    //create target page
    // awk ' { print "variableContainer.append(qMakePair(QLatin1String(\"" $0 "\"), QLatin1String(\"" $0 "\")));"} ' cStandards
    // use the above awk script replace fputype with file having list of strings

    QList< QPair<QString, QString> > variableContainer;
    variableContainer.append(qMakePair(QLatin1String("arm7tdmi"), QLatin1String("arm7tdmi")));
    variableContainer.append(qMakePair(QLatin1String("cortex-m4"), QLatin1String("cortex-m4")));
    ui->cpuWidget->init(tr("Target processor:"), variableContainer, -1);

    variableContainer.clear();
    variableContainer.append(qMakePair(QLatin1String("Thumb (-mthumb)"), QLatin1String("-mthumb")));
    variableContainer.append(qMakePair(QLatin1String("Arm (-marm)"), QLatin1String("-marm")));

    ui->instructionSetWidget->init(tr("Instruction set:"), variableContainer, -1);

    variableContainer.clear();
    variableContainer.append(qMakePair(tr("Software implementation (soft)"), QLatin1String("soft")));
    variableContainer.append(qMakePair(tr("Mix HW/SW implementation (softfp)"), QLatin1String("softfp")));
    variableContainer.append(qMakePair(tr("Hardware implementation (hard)"), QLatin1String("hard")));
    Q_FOREACH(const Pair pair, variableContainer)
        ui->comboBoxFpuType->addItem(pair.first, pair.second);

    variableContainer.clear();
    variableContainer.append(qMakePair(QLatin1String("vfp"), QLatin1String("vfp")));
    variableContainer.append(qMakePair(QLatin1String("vfpv3"), QLatin1String("vfpv3")));
    variableContainer.append(qMakePair(QLatin1String("vfpv3-fp16"), QLatin1String("vfpv3-fp16")));
    variableContainer.append(qMakePair(QLatin1String("vfpv3-d16"), QLatin1String("vfpv3-d16")));
    variableContainer.append(qMakePair(QLatin1String("vfpv3-d16-fp16"), QLatin1String("vfpv3-d16-fp16")));
    variableContainer.append(qMakePair(QLatin1String("vfpv3xd"), QLatin1String("vfpv3xd")));
    variableContainer.append(qMakePair(QLatin1String("vfpv3xd-fp16"), QLatin1String("vfpv3xd-fp16")));
    variableContainer.append(qMakePair(QLatin1String("neon"), QLatin1String("neon")));
    variableContainer.append(qMakePair(QLatin1String("neon-fp16"), QLatin1String("neon-fp16")));
    variableContainer.append(qMakePair(QLatin1String("vfpv4"), QLatin1String("vfpv4")));
    variableContainer.append(qMakePair(QLatin1String("vfpv4-d16"), QLatin1String("vfpv4-d16")));
    variableContainer.append(qMakePair(QLatin1String("fpv4-sp-d16"), QLatin1String("fpv4-sp-d16")));
    variableContainer.append(qMakePair(QLatin1String("neon-vfpv4"), QLatin1String("neon-vfpv4")));
    variableContainer.append(qMakePair(QLatin1String("fpv5-d16"), QLatin1String("fpv5-d16")));
    variableContainer.append(qMakePair(QLatin1String("fpv5-sp-d16"), QLatin1String("fpv5-sp-d16")));
    variableContainer.append(qMakePair(QLatin1String("fp-armv8"), QLatin1String("fp-armv8")));
    variableContainer.append(qMakePair(QLatin1String("neon-fp-armv8"), QLatin1String("neon-fp-armv8")));
    variableContainer.append(qMakePair(QLatin1String("crypto-neon-fp-armv8"), QLatin1String("crypto-neon-fp-armv8")));

    ui->fpuWidget->init(tr("Floating point unit:"), variableContainer, -1);

    variableContainer.clear();
    variableContainer.append(qMakePair(QLatin1String("None"), QString()));
    variableContainer.append(qMakePair(QLatin1String("Minimum (-g1)"), QLatin1String("-g1")));
    variableContainer.append(qMakePair(QLatin1String("Default (-g)"), QLatin1String("-g")));
    variableContainer.append(qMakePair(QLatin1String("Maximum (-g3)"), QLatin1String("-g3")));
    ui->assemblerDebugWidget->init(QLatin1String("Debug level:"), variableContainer, -1);
    ui->cDebgWidget->init(QLatin1String("Debug level:"), variableContainer, -1);

    variableContainer.clear();
    variableContainer.append(qMakePair(QLatin1String("None"), QString()));
    variableContainer.append(qMakePair(QLatin1String("Level 1 (-O1)"), QLatin1String("-O1")));
    variableContainer.append(qMakePair(QLatin1String("Level 2 (-O2)"), QLatin1String("-O2")));
    variableContainer.append(qMakePair(QLatin1String("Level 3 (-O3)"), QLatin1String("-O3")));
    variableContainer.append(qMakePair(QLatin1String("Optimize for size (-Os)"), QLatin1String("-Os")));
    variableContainer.append(qMakePair(QLatin1String("Optimize for speed (-Ofast)"), QLatin1String("-Ofast")));
    ui->COptimizationWidget->init(QLatin1String("Optimization level:"), variableContainer, -1);
    //init in order to apply project setting


    // C standards
    variableContainer.append(qMakePair(QLatin1String("c90"), QLatin1String("c90")));
    variableContainer.append(qMakePair(QLatin1String("c89"), QLatin1String("c89")));
    variableContainer.append(qMakePair(QLatin1String("iso9899:1990"), QLatin1String("iso9899:1990")));
    variableContainer.append(qMakePair(QLatin1String("iso9899:199409"), QLatin1String("iso9899:199409")));
    variableContainer.append(qMakePair(QLatin1String("c99"), QLatin1String("c99")));
    variableContainer.append(qMakePair(QLatin1String("c9x"), QLatin1String("c9x")));
    variableContainer.append(qMakePair(QLatin1String("iso9899:1999"), QLatin1String("iso9899:1999")));
    variableContainer.append(qMakePair(QLatin1String("iso9899:199x"), QLatin1String("iso9899:199x")));
    variableContainer.append(qMakePair(QLatin1String("c11"), QLatin1String("c11")));
    variableContainer.append(qMakePair(QLatin1String("c1x"), QLatin1String("c1x")));
    variableContainer.append(qMakePair(QLatin1String("iso9899:2011"), QLatin1String("iso9899:2011")));
    variableContainer.append(qMakePair(QLatin1String("gnu90"), QLatin1String("gnu90")));
    variableContainer.append(qMakePair(QLatin1String("gnu89"), QLatin1String("gnu89")));
    variableContainer.append(qMakePair(QLatin1String("gnu99"), QLatin1String("gnu99")));
    variableContainer.append(qMakePair(QLatin1String("gnu9x"), QLatin1String("gnu9x")));
    variableContainer.append(qMakePair(QLatin1String("gnu11"), QLatin1String("gnu11")));
    variableContainer.append(qMakePair(QLatin1String("gnu1x"), QLatin1String("gnu1x")));
    ui->cStandard->init(tr("C standard:"), variableContainer, -1);

    //c++ standards
    variableContainer.append(qMakePair(QLatin1String("c++98"), QLatin1String("c++98")));
    variableContainer.append(qMakePair(QLatin1String("c++03"), QLatin1String("c++03")));
    variableContainer.append(qMakePair(QLatin1String("gnu++98"), QLatin1String("gnu++98")));
    variableContainer.append(qMakePair(QLatin1String("gnu++03"), QLatin1String("gnu++03")));
    variableContainer.append(qMakePair(QLatin1String("c++11"), QLatin1String("c++11")));
    variableContainer.append(qMakePair(QLatin1String("c++0x"), QLatin1String("c++0x")));
    variableContainer.append(qMakePair(QLatin1String("gnu++11"), QLatin1String("gnu++11")));
    variableContainer.append(qMakePair(QLatin1String("gnu++0x"), QLatin1String("gnu++0x")));
    variableContainer.append(qMakePair(QLatin1String("c++14"), QLatin1String("c++14")));
    variableContainer.append(qMakePair(QLatin1String("c++1y"), QLatin1String("c++1y")));
    variableContainer.append(qMakePair(QLatin1String("gnu++14"), QLatin1String("gnu++14")));
    variableContainer.append(qMakePair(QLatin1String("gnu++1y"), QLatin1String("gnu++1y")));
    variableContainer.append(qMakePair(QLatin1String("c++1z"), QLatin1String("c++1z")));
    variableContainer.append(qMakePair(QLatin1String("gnu++1z"), QLatin1String("gnu++1z")));
    ui->cppStandard->init(tr("C++ standard:"), variableContainer, -1);

    ArmProject* project = m_projectBuildSetting.project();
    if(project)
    {
        Q_FOREACH(const QString& filePath, project->linkerFiles())
            ui->cmbLLinkerFile->addItem(project->relativeFilePath(filePath), filePath);
    }

    ui->toolButtonLAdd->setIcon(QIcon(QLatin1String(":/images/add.png")));
    ui->toolButtonLAdd->setToolTip(tr("Add static library"));
    ui->toolButtonLRemove->setIcon(QIcon(QLatin1String(":/images/remove.png")));
    ui->toolButtonLRemove->setToolTip(tr("Remove selected library"));
    init();
}

ProjectBuildSettingPageWidget::~ProjectBuildSettingPageWidget()
{
    delete ui;
}

QWidget *ProjectBuildSettingPageWidget::widget()
{
    return this;
}

void ProjectBuildSettingPageWidget::apply()
{
    //apply arm target setting
    m_projectBuildSetting.setTargetSetting(m_cpuTargetSetting);
    m_projectBuildSetting.setAssemblerSetting(m_assemblerSetting);
    m_projectBuildSetting.setCCompilationSetting(m_cCompilationSetting);
    m_projectBuildSetting.setLinkerSetting(m_linkerSetting);

    //other setting
    OtherSetting otherSetting;
    otherSetting.createBin = ui->cbOCreateBin->isChecked();
    otherSetting.createHex = ui->cbOCreateHex->isChecked();
    m_projectBuildSetting.setOtherSetting(otherSetting);
}

void ProjectBuildSettingPageWidget::init()
{

    //init target setting
    const ArmTargetSetting& targetSetting = m_projectBuildSetting.targetCpuSetting();
    ui->cpuWidget->setCurrentValue(targetSetting.cpu);
    ui->instructionSetWidget->setCurrentValue(targetSetting.instructionSet);
    ui->comboBoxFpuType->setCurrentIndex(ui->comboBoxFpuType->findData(targetSetting.floatAbi));
    ui->checkBoxInterWork->setChecked(targetSetting.useInterWorking);
    ui->fpuWidget->setCurrentValue(targetSetting.fpu);
    slotTargetSettingChanged();//initialise local value
    connectTargetPageConnections();

    //assembler function
    const AssemblerSetting& asSettings = m_projectBuildSetting.assemblerSetting();
    ui->assemblerDebugWidget->setCurrentValue(asSettings.debugFlag);
    ui->checkBoxAssembler->setChecked(asSettings.showWarning);
    slotAssemblerSettingChanged(); //initialise local value
    connect(ui->assemblerDebugWidget, SIGNAL(valueChanged()), this, SLOT(slotAssemblerSettingChanged()));
    connect(ui->checkBoxAssembler, SIGNAL(toggled(bool)), this, SLOT(slotAssemblerSettingChanged()));


    //CCompilation
    const CCompilationSetting& cSetting = m_projectBuildSetting.cCompilationSetting();
    ui->cDebgWidget->setCurrentValue(cSetting.debugFlag);
    ui->COptimizationWidget->setCurrentValue(cSetting.optimizationFlag);
    ui->cbCShowAllWarning->setChecked(cSetting.showAllWarnings);
    ui->cbRemoveDeadCode->setChecked(cSetting.removeDeadCode);
    ui->cbRemoveDeadData->setChecked(cSetting.removeDeadData);
    ui->leCExtraFlags->setText(cSetting.extraFlags);
    ui->cStandard->setCurrentValue(cSetting.cStd);
    ui->cppStandard->setCurrentValue(cSetting.cppStd);
    slotCSettingSettingChanged();//initialize local values
    connectCPageConnections();

    //linker Setting
    const LinkerSetting& linkerSetting = m_projectBuildSetting.linkerSetting();
    int index = ui->cmbLLinkerFile->findData(linkerSetting.linkerFile);
    if(index == -1 && ui->cmbLLinkerFile->count())
        index = 0;

    ui->cmbLLinkerFile->setCurrentIndex(index);
    ui->cbLRemoveDeadCode->setChecked(linkerSetting.removeDeadCode);
    ui->cbLNoDefaultLib->setChecked(linkerSetting.doNotUseDefaultLib);
    ui->cbLNoSharedLib->setChecked(linkerSetting.doNotUseSharedLib);
    ui->cbLNoStartUpFile->setChecked(linkerSetting.doNotUseStartUpFile);
    ui->cbLNoStandardLib->setChecked(linkerSetting.doNotUseStandardLib);
    ui->cbLNanoLib->setChecked(linkerSetting.linkWithNewLibNano);
    ui->leLExtraFlags->setText(linkerSetting.extraFlags);
    Q_FOREACH(const QString& staticLib, linkerSetting.staticLibraries)
        ui->listWidgetLStaticLibs->addItem(staticLib);
    slotLinkerSettingChanged(); //initialise local values
    connectLinkerPageConnections();

    //extra page
    const OtherSetting& otherSetting = m_projectBuildSetting.otherSetting();
    ui->cbOCreateBin->setChecked(otherSetting.createBin);
    ui->cbOCreateHex->setChecked(otherSetting.createHex);

    connect(ui->tabWidget, SIGNAL(currentChanged(int)), this, SLOT(slotCurrenWidgetChanged()));
}

void ProjectBuildSettingPageWidget::connectTargetPageConnections()
{
    connect(ui->cpuWidget, SIGNAL(valueChanged()), this,SLOT(slotTargetSettingChanged()));
    connect(ui->fpuWidget, SIGNAL(valueChanged()), this,SLOT(slotTargetSettingChanged()));
    connect(ui->instructionSetWidget, SIGNAL(valueChanged()), this,SLOT(slotTargetSettingChanged()));
    connect(ui->checkBoxInterWork, SIGNAL(toggled(bool)), this,SLOT(slotTargetSettingChanged()));
    connect(ui->comboBoxFpuType, SIGNAL(currentIndexChanged(QString)), this,SLOT(slotTargetSettingChanged()));
}

void ProjectBuildSettingPageWidget::connectCPageConnections()
{
    connect(ui->cDebgWidget, SIGNAL(valueChanged()), this, SLOT(slotCSettingSettingChanged()));
    connect(ui->COptimizationWidget, SIGNAL(valueChanged()), this, SLOT(slotCSettingSettingChanged()));
    connect(ui->cbCShowAllWarning, SIGNAL(toggled(bool)), this, SLOT(slotCSettingSettingChanged()));
    connect(ui->cbRemoveDeadCode, SIGNAL(toggled(bool)), this, SLOT(slotCSettingSettingChanged()));
    connect(ui->cbRemoveDeadData, SIGNAL(toggled(bool)), this, SLOT(slotCSettingSettingChanged()));
    connect(ui->leCExtraFlags, SIGNAL(textChanged(QString)), this, SLOT(slotCSettingSettingChanged()));
    connect(ui->cStandard, SIGNAL(valueChanged()), this, SLOT(slotCSettingSettingChanged()));
    connect(ui->cppStandard, SIGNAL(valueChanged()), this, SLOT(slotCSettingSettingChanged()));
}

void ProjectBuildSettingPageWidget::connectLinkerPageConnections()
{
    connect(ui->cmbLLinkerFile, SIGNAL(currentIndexChanged(int)), this, SLOT(slotLinkerSettingChanged()));
    connect(ui->cbLNoDefaultLib, SIGNAL(toggled(bool)), this, SLOT(slotLinkerSettingChanged()));
    connect(ui->cbLNoSharedLib, SIGNAL(toggled(bool)), this, SLOT(slotLinkerSettingChanged()));
    connect(ui->cbLNoStandardLib, SIGNAL(toggled(bool)), this, SLOT(slotLinkerSettingChanged()));
    connect(ui->cbLNoStartUpFile, SIGNAL(toggled(bool)), this, SLOT(slotLinkerSettingChanged()));
    connect(ui->cbLRemoveDeadCode, SIGNAL(toggled(bool)), this, SLOT(slotLinkerSettingChanged()));
    connect(ui->cbLNanoLib, SIGNAL(toggled(bool)), this, SLOT(slotLinkerSettingChanged()));
    connect(ui->leLExtraFlags, SIGNAL(textChanged(QString)), this, SLOT(slotLinkerSettingChanged()));
}

ArmTargetSetting ProjectBuildSettingPageWidget::targetSettingFromUi() const
{
    ArmTargetSetting targetSetting;
    targetSetting.cpu = ui->cpuWidget->value();
    targetSetting.instructionSet = ui->instructionSetWidget->value();
    targetSetting.floatAbi = ui->comboBoxFpuType->currentData().toString();
    targetSetting.useInterWorking = ui->checkBoxInterWork->isChecked();
    targetSetting.fpu = ui->fpuWidget->value();
    return targetSetting;
}

AssemblerSetting ProjectBuildSettingPageWidget::assemblerSettingFromUi() const
{
    AssemblerSetting assemblerSetting;
    assemblerSetting.debugFlag = ui->assemblerDebugWidget->value();
    assemblerSetting.showWarning = ui->checkBoxAssembler->isChecked();
    return assemblerSetting;
}

CCompilationSetting ProjectBuildSettingPageWidget::cCompilationSettingFromUi() const
{
    CCompilationSetting cSetting;
    cSetting.debugFlag = ui->cDebgWidget->value();
    cSetting.optimizationFlag = ui->COptimizationWidget->value();
    cSetting.showAllWarnings = ui->cbCShowAllWarning->isChecked();
    cSetting.removeDeadCode = ui->cbRemoveDeadCode->isChecked();
    cSetting.removeDeadData = ui->cbRemoveDeadData->isChecked();
    cSetting.extraFlags = ui->leCExtraFlags->text();
    cSetting.cStd = ui->cStandard->value();
    cSetting.cppStd = ui->cppStandard->value();
    return cSetting;
}

LinkerSetting ProjectBuildSettingPageWidget::linkerSettingFromUi() const
{
    LinkerSetting linkerSetting;
    linkerSetting.linkerFile = ui->cmbLLinkerFile->currentData().toString();
    linkerSetting.removeDeadCode = ui->cbLRemoveDeadCode->isChecked();
    linkerSetting.doNotUseDefaultLib = ui->cbLNoDefaultLib->isChecked();
    linkerSetting.doNotUseSharedLib = ui->cbLNoSharedLib->isChecked();
    linkerSetting.doNotUseStandardLib = ui->cbLNoStandardLib->isChecked();
    linkerSetting.doNotUseStartUpFile = ui->cbLNoStartUpFile->isChecked();
    linkerSetting.linkWithNewLibNano = ui->cbLNanoLib->isChecked();
    linkerSetting.extraFlags = ui->leLExtraFlags->text();
    int libCount = ui->listWidgetLStaticLibs->count();
    for (int i = 0; i < libCount; i++)
    {
        QListWidgetItem* item = ui->listWidgetLStaticLibs->item(i);
        linkerSetting.staticLibraries.append(item->text());
    }

    return linkerSetting;
}

void ProjectBuildSettingPageWidget::refreshAssemblerPageCommand()
{
    QLatin1Char space(' ');
    QString str = m_projectBuildSetting.toolChain().cCompiler() + QLatin1String(" -c ");
    str += m_cpuTargetSetting.flags() + space + m_assemblerSetting.flags();
    str += QLatin1String(" -o %1.o %1.s");
    str = str.arg(tr("example"));
    ui->textEditAssembler->setText(str);
}

void ProjectBuildSettingPageWidget::refreshCCompilationPageCommand()
{
    QString str = m_projectBuildSetting.toolChain().cCompiler() + QLatin1String(" -c ");
    str += m_cpuTargetSetting.flags() + QLatin1String(" ${DEFINES}  ${INCLUDES} ") + m_cCompilationSetting.flags();
    str += QLatin1String(" -o %1.o %1.c");
    str = str.arg(tr("example"));
    ui->cTextEdit->setPlainText(str);
}

void ProjectBuildSettingPageWidget::refreshLinkerPageCommand()
{
    QLatin1Char space(' ');
    QString str = m_projectBuildSetting.toolChain().cCompiler() + space;
    str += QLatin1String(" {OBJECTS} ");
    str += m_cpuTargetSetting.flags() + space ;
    str += m_linkerSetting.flags();
    ui->linkerTextEdit->setPlainText(str);
}

void ProjectBuildSettingPageWidget::slotCurrenWidgetChanged()
{
    QWidget* w = ui->tabWidget->currentWidget();
    if(w)
    {
        if(w == ui->assemblerTab)
            refreshAssemblerPageCommand();
        else if(w == ui->cCompilationPage)
            refreshCCompilationPageCommand();
        else if(w == ui->linkerPage)
            refreshLinkerPageCommand();
    }
}

void ProjectBuildSettingPageWidget::slotTargetSettingChanged()
{
    m_cpuTargetSetting = targetSettingFromUi();
    ui->labelTargetFlags->setText(QLatin1String("<b>") +
                                  m_cpuTargetSetting.flags() +
                                  QLatin1String("</b>"));
}

void ProjectBuildSettingPageWidget::slotAssemblerSettingChanged()
{
    m_assemblerSetting = assemblerSettingFromUi();
    refreshAssemblerPageCommand();
}

void ProjectBuildSettingPageWidget::slotCSettingSettingChanged()
{
    m_cCompilationSetting = cCompilationSettingFromUi();
    refreshCCompilationPageCommand();
}

void ProjectBuildSettingPageWidget::slotLinkerSettingChanged()
{
    m_linkerSetting = linkerSettingFromUi();
    refreshLinkerPageCommand();
}

void ProjectBuildSettingPageWidget::on_toolButtonLAdd_clicked()
{
    QStringList libFiles =  QFileDialog::getOpenFileNames(this, tr("Select static libs"), QDir::homePath());
    if(libFiles.count())
    {
        QStringList libsToAdd;
        Q_FOREACH(const QString lib, libFiles)
        {
            if(!m_linkerSetting.staticLibraries.contains(lib))
                libsToAdd.append(lib);
        }
        ui->listWidgetLStaticLibs->addItems(libsToAdd);
        if(libsToAdd.count())
            slotLinkerSettingChanged();
    }
}

void ProjectBuildSettingPageWidget::on_toolButtonLRemove_clicked()
{
    QList<QListWidgetItem*> seletedItems = ui->listWidgetLStaticLibs->selectedItems();
    Q_FOREACH(QListWidgetItem* item, seletedItems)
    {
        ui->listWidgetLStaticLibs->takeItem(ui->listWidgetLStaticLibs->row(item));
        delete item;
    }

    if(seletedItems.count())
        slotLinkerSettingChanged();
}

} // namespace Internal
} // namespace ArmProjectManage

