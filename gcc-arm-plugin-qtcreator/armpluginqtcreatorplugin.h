#ifndef ARMPLUGINQTCREATOR_H
#define ARMPLUGINQTCREATOR_H

#include "armpluginqtcreator_global.h"

#include <extensionsystem/iplugin.h>
#include <projectexplorer/project.h>
#include <projectexplorer/projectnodes.h>

namespace ArmPluginQtCreator {
namespace Internal {

class ArmPluginQtCreatorPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QtCreatorPlugin" FILE "ArmPluginQtCreator.json")

public:
    ArmPluginQtCreatorPlugin();
    ~ArmPluginQtCreatorPlugin();

    bool initialize(const QStringList &arguments, QString *errorString);
    void extensionsInitialized();
    ShutdownFlag aboutToShutdown();

private slots:
    void updateContextActions(ProjectExplorer::Project *project);
    void slotAddIncludePaths();
    void slotAddExistingFiles();
    void slotShowProjectPropertyDialog();

private:
    ProjectExplorer::Project* m_currentProject;
};

} // namespace Internal
} // namespace ArmPluginQtCreator

#endif // ARMPLUGINQTCREATOR_H

