#include "comboselectionutilitywidget.h"
#include "ui_comboselectionutilitywidget.h"

namespace ArmPluginQtCreator {
namespace Internal {

typedef QPair<QString, QString> Pair;

ComboSelectionUtilityWidget::ComboSelectionUtilityWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ComboSelectionUtilityWidget)
{
    ui->setupUi(this);
}

ComboSelectionUtilityWidget::~ComboSelectionUtilityWidget()
{
    delete ui;
}

void ComboSelectionUtilityWidget::init(const QString &name, const QList<QPair<QString, QString> > &items, int currentIndex)
{
    ui->labelValue->setVisible(false);
    ui->label->setText(name);
    if(currentIndex < 0)
        currentIndex = 0;

    QList< QPair<QString, QString> > allItems = items;
    allItems.append(qMakePair(tr("Other"), QString()));

    ui->comboBox->blockSignals(true);
    Q_FOREACH(const Pair& pair, allItems)
        ui->comboBox->addItem(pair.first, pair.second);
    ui->comboBox->blockSignals(false);
    ui->comboBox->setCurrentIndex(currentIndex);
    on_comboBox_currentIndexChanged(ui->comboBox->currentText());
    connect(ui->lineEdit, SIGNAL(textChanged(QString)), this, SIGNAL(valueChanged()));
}

QString ComboSelectionUtilityWidget::value() const
{
    if(ui->lineEdit->isEnabled())
        return ui->lineEdit->text();

    return ui->comboBox->currentData().toString();
}

void ComboSelectionUtilityWidget::setCurrentValue(QString userData)
{
    int index = ui->comboBox->findData(userData);
    if(index >= 0)
        ui->comboBox->setCurrentIndex(index);
    else
    {
        int lastIndex = ui->comboBox->count() -1;
        ui->comboBox->setCurrentIndex(lastIndex);
        ui->comboBox->setItemData(lastIndex, userData);
        ui->lineEdit->setText(userData);
        on_comboBox_currentIndexChanged(ui->comboBox->currentText());
    }
}

void ComboSelectionUtilityWidget::on_comboBox_currentIndexChanged(const QString &arg1)
{
    bool enableLineEdit = false;
    if(arg1 == tr("Other"))
        enableLineEdit = true;

    ui->lineEdit->setText(ui->comboBox->currentData().toString());
    ui->lineEdit->setEnabled(enableLineEdit);
    ui->labelValue->setVisible(enableLineEdit);
    //emit valueChanged();
}

} // namespace Internal
} // namespace ArmPluginQtCreator

