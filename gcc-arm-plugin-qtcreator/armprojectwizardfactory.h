//#ifndef ARMPROJECTWIZARDFACTORY_H
//#define ARMPROJECTWIZARDFACTORY_H

//#include <projectexplorer/baseprojectwizarddialog.h>
//#include <coreplugin/basefilewizardfactory.h>
//#include <projectexplorer/kitchooser.h>
//#include <projectexplorer/targetsetuppage.h>
//#include <coreplugin/generatedfile.h>
//#include <utils/projectintropage.h>

//namespace ArmPluginQtCreator {
//namespace Internal {

//class ArmProjectWizard : public ProjectExplorer::BaseProjectWizardDialog
//{
//    Q_OBJECT
//    friend class ArmProjectWizardFactory;
//public:
//    ArmProjectWizard(QWidget *parent, const Core::WizardDialogParameters &parameters);
//    ~ArmProjectWizard();

//private slots:
//    void onProjectParameterChanged();

//private:
//    ProjectExplorer::TargetSetupPage* m_targetSetupPage;
//};

//class ArmProjectWizardFactory : public Core::BaseFileWizardFactory
//{
//public:
//    ArmProjectWizardFactory();
//    ~ArmProjectWizardFactory();

//protected:
//    Core::BaseFileWizard *create(QWidget *parent, const Core::WizardDialogParameters &parameters) const;
//    Core::GeneratedFiles generateFiles(const QWizard *w, QString *errorMessage) const;
//    bool postGenerateFiles(const QWizard *w, const Core::GeneratedFiles &l, QString *errorMessage);
//};

//} // namespace Internal
//} // namespace ArmPluginQtCreator
//#endif // ARMPROJECTWIZARDFACTORY_H
