#ifndef ARMPROJECTGENERICSTEP_H
#define ARMPROJECTGENERICSTEP_H

#include <projectexplorer/buildstep.h>
#include <projectexplorer/processparameters.h>
#include <projectexplorer/ioutputparser.h>
#include <projectexplorer/kitinformation.h>
#include <utils/qtcprocess.h>
#include "armprojectbuildsetting.h"

namespace ArmPluginQtCreator {
namespace Internal {

class ArmProject;
class ArmProjectGenericBuildStep : public ProjectExplorer::BuildStep
{
    Q_OBJECT
    friend class ArmProjectBuildStepFactory;
public:
    ArmProjectGenericBuildStep(ProjectExplorer::BuildStepList *parent);
    ~ArmProjectGenericBuildStep();

    bool init(QList<const BuildStep *> &earlierSteps);
    void run(QFutureInterface<bool> &fi);

    ProjectExplorer::BuildStepConfigWidget *createConfigWidget();
    QVariantMap toMap() const;

    void setOutputParser(ProjectExplorer::IOutputParser *parser);
    void appendOutputParser(ProjectExplorer::IOutputParser *parser);
    ProjectExplorer::IOutputParser *outputParser() const;
    bool runInGuiThread() const;
    void setClean(bool clean);
    bool isCleanStep() const;

protected:
    bool fromMap(const QVariantMap &map);
    ArmProjectGenericBuildStep(ProjectExplorer::BuildStepList *parent, ArmProjectGenericBuildStep *bs);

private:
    void cleanUp(bool result);
    void emitFaultyConfigurationMessage();
    void extractBuildInformationFromProject();
    void startNextSourceCompilationProcess();
    void startCleanStep();
    void startLinkingStep();
    void clearRunTimeVariables();
    void startObjcopyProcess();
    void startProcessFromInternalParams();
    void cleanProcessOnFailure();

private slots:
    void processReadyReadStdOutput();
    void processReadyReadStdError();
    void slotProcessFinished(int, QProcess::ExitStatus);
    void outputAdded(const QString &string, ProjectExplorer::BuildStep::OutputFormat format);
    virtual void processStarted();
    virtual void processFinished(int exitCode, QProcess::ExitStatus status);
    virtual void processStartupFailed();
    virtual bool processSucceeded(int exitCode, QProcess::ExitStatus status);
    virtual void stdOutput(const QString &line);
    virtual void stdError(const QString &line);
    void checkForCancel();
    void taskAdded(const ProjectExplorer::Task &task);

private:
    ProjectExplorer::ProcessParameters m_param;
    ProjectExplorer::IOutputParser* m_outputParserChain;
    QFutureInterface<bool>* m_futureInterface;
    Utils::QtcProcess *m_process;
    QTimer *m_timer;
    bool m_killProcess;
    bool m_cleanStep;
    QHash<QString, QString> m_filesHash; //key is file to be compiled  and value is object file
    ProjectExplorer::ToolChain *m_toolChain;
    QString m_cppCompilerCommand;
    QString m_cCompilerCommand;
    QString m_objcopyCommand;
    QString m_defines;
    QString m_includepaths;
    ArmProjectBuildSetting* m_buildSetting;
    ArmProject* m_project;
    int m_CompiledFileIndex;
    int m_sourceFileToCompileCount;
    int m_afterCompileStatus;
    bool m_haveCppFiles;
};

//////////////////////////////////////////////////////////////////////////////////////////
/// \brief The ReadOnlyGenericStepWidget class
////////////////////////////////////////////////////////////////////////////////////////////

class ReadOnlyGenericStepWidget : public ProjectExplorer::BuildStepConfigWidget
{
    Q_OBJECT

public:
    ReadOnlyGenericStepWidget(ArmProjectGenericBuildStep* step) : m_step(step){}
    ~ReadOnlyGenericStepWidget() {}
    virtual QString summaryText() const {
        return m_step->isCleanStep() ? tr("Clean Step") : tr("Build Step using internal builder");
    }
    virtual QString additionalSummaryText() const {
        return tr("Internal builder will be used to build or clean project instead of make.");
    }
    virtual QString displayName() const {
        return m_step->isCleanStep() ? tr("Clean Step") : tr("Build Step using internal builder");
    }
    virtual bool showWidget() const { return true;}

private:
    ArmProjectGenericBuildStep* m_step;
};


//////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The ArmProjectBuildStepFactory class
///////////////////////////////////////////////////////////////////////////////////////////////


class ArmProjectBuildStepFactory : public ProjectExplorer::IBuildStepFactory
{
    Q_OBJECT

public:
    explicit ArmProjectBuildStepFactory(QObject *parent = 0);
    QList<ProjectExplorer::BuildStepInfo>
        availableSteps(ProjectExplorer::BuildStepList *parent) const override;

    ProjectExplorer::BuildStep *create(ProjectExplorer::BuildStepList *parent, Core::Id id) override;
    ProjectExplorer::BuildStep *clone(ProjectExplorer::BuildStepList *parent,
                                      ProjectExplorer::BuildStep *source) override;
};

} // namespace Internal
} // namespace ArmProjectManager

#endif // ARMPROJECTGENERICSTEP_H
