#ifndef ARMPROJECTSETTINGDIALOG_H
#define ARMPROJECTSETTINGDIALOG_H

#include <QDialog>
#include <QAbstractListModel>
#include "ISettingPageWidget.h"
#include "armprojectbuildsetting.h"

class QStackedLayout;
namespace Ui {
class ArmProjectSettingDialog;
}

namespace ArmPluginQtCreator {
namespace Internal {

class SettingPageWidgetsModel : public QAbstractListModel
{
    Q_OBJECT
public:
    SettingPageWidgetsModel(QObject* parent);
    ~SettingPageWidgetsModel();

    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    void addPage(ISettingPageWidget* page);
    ISettingPageWidget* settingPage(int index) const;
    const QList<ISettingPageWidget *> &allSettingPages() const;

private:
    QList<ISettingPageWidget*> m_pages;
};

class ArmProjectSettingDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ArmProjectSettingDialog(ArmProjectBuildSetting& buildSetting, QWidget *parent = 0);
    ~ArmProjectSettingDialog();


private:
    void addSettingPageWidget(ISettingPageWidget* page);

private slots:
    void slotCurrentIndexChanged(QModelIndex index);
    void slotAccept();
    void slotApply();

private:
    Ui::ArmProjectSettingDialog *ui;
    SettingPageWidgetsModel* m_model;

};

} // namespace Internal
} // namespace ArmProjectManager

#endif // ARMPROJECTSETTINGDIALOG_H
